import os
import json
from colorama import init
from termcolor import colored
import time
import click
import docker

init(autoreset=True, convert=False, strip=False)


@click.command()
@click.option('--action', type=click.Choice(['run', 'test', 'build']), default='run')
def main(action):
    print('Building the Docker image...')
    image_name = 'celldoctor'
    from docker.client import APIClient
    DOCKER_HOST = 'tcp://127.0.0.1:2375'
    if os.environ.get("DOCKER_HOST"):
        DOCKER_HOST = os.environ.get("DOCKER_HOST")

    cli = APIClient(base_url=DOCKER_HOST)
    with open("./Dockerfile", 'rb') as f:
        for response in cli.build(fileobj=f, tag=image_name):
            lines = response.decode('UTF-8').split('\n')
            for s in lines:
                try:
                    print(json.loads(s)["stream"], end='')
                except:
                    pass

    # log_generator = client.images.build(path="./", tag=('%s' % image_name), quiet=False)
    # for msg in log_generator:
    #     print(msg.get('stream', ''), end='')

    if action == "build":
        return

    client = docker.from_env()

    if action == "run":
        run_notebook(client, image_name)

    if action == "test":
        dirpath = os.getcwd()
        volumes = {
            dirpath + '/Segmenter': {'bind': '/Segmenter', 'mode': 'ro'}
        }

        # container = cli.create_container(
        container = client.containers.run(
            image=image_name,
            command='python -m unittest discover --verbose . \"test_*.py\"',
            working_dir='/Segmenter/tests/',
            ports={},
            volumes=volumes,
            environment={'PYTHONUNBUFFERED': 0},
            privileged=True,
            auto_remove=True,
            stream=True,
            stdout=True,
            stderr=True,
            # detach=True
        )
        # response = cli.start(container=container.get('Id'))
        # while True:
        #     try:
        #         t = container.logs(since=int(time.time()) - 1)
        #         if t:
        #             print(t.decode('UTF-8'))
        #         time.sleep(1)
        #     except docker.errors.NotFound as err:
        #         break
        try:
            container.stop()
        except:
            pass
        finally:
            print(colored('Docker run had ended.', 'blue'))


def run_notebook(client, image_name):
    dirpath = os.getcwd()
    volumes = {
        dirpath + '/Notebooks/': {'bind': '/workdir', 'mode': 'rw'},
        dirpath + '/Segmenter': {'bind': '/workdir/Segmenter', 'mode': 'ro'},
        # dirpath + '/Code': {'bind': '/workdir/Code', 'mode': 'ro'}
    }
    if os.path.exists('vol_mapping.json'):
        with open('vol_mapping.json') as vfn:
            vol_mapping = json.load(vfn)
        for src, dst_name in vol_mapping.items():
            if not os.path.exists(src):
                print(colored(f'Could not find {src}!  Skipping path mapping.', 'red'))
                continue
            if dst_name == "":
                vol_mapping[src] = '/workdir/' + os.path.basename(src)
            volumes[src] = {'bind': vol_mapping[src], 'mode': 'rw'}
    print(colored('Running Docker...', 'blue'))
    container = client.containers.run(
        image=image_name, command='start-notebook.sh',
        ports={
            '8888/tcp': '10000/tcp'},
        volumes=volumes,
        environment={'PYTHONUNBUFFERED': 0},
        privileged=True,
        auto_remove=True,
        stream=True,
        stdout=True,
        stderr=True,
        detach=True
    )
    while True:
        try:
            t = container.logs(since=int(time.time()) - 1)
            if t:
                print(t.decode('UTF-8'))
            time.sleep(1)
        except docker.errors.NotFound as err:
            break
    try:
        container.stop()
    except:
        pass
    finally:
        print(colored('Docker run had ended.', 'blue'))


if __name__ == '__main__':
    main()
