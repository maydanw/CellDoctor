require(["nbextensions/snippets_menu/main"], function (snippets_menu) {
    console.log('Loading `snippets_menu` customizations from `custom.js`');
    var horizontal_line = '---';
    var my_favorites = {
        'name' : 'My favorites',
        'sub-menu' : [
            {
                'name' : 'tqdm loop',
                'snippet' : ["from tqdm import tqdm_notebook",
                             "from time import sleep",
                             "",
                             "for j in tqdm_notebook(range(100), desc='loop1'):",
                             "    sleep(0.1)"
                            ],
            },
            {
                'name' : 'QGrid partly editable',
                'snippet' : ["import qgrid",
                             "col_opts = { 'editable': False }",
                             "col_defs = { 'A': { 'editable': True } }",
                             "qgrid.show_grid(df, column_options=col_opts, column_definitions=col_defs)"],
            },
            {
                'name' : 'TableDisplay',
                'snippet' : ["from beakerx import *",
                             "TableDisplay(df.sample(30))"]
            },
        ],
    };
    snippets_menu.options['menus'] = snippets_menu.default_menus;
    snippets_menu.options['menus'][0]['sub-menu'].push(horizontal_line);
    snippets_menu.options['menus'][0]['sub-menu'].push(my_favorites);
    console.log('Loaded `snippets_menu` customizations from `custom.js`');
});