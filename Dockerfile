FROM jupyter/tensorflow-notebook
LABEL MAINTAINER="Maydan Wienreb <maydanw@gmail.com>"

COPY ./DockerConfigurations/jupyter_notebook_config.py /etc/jupyter/jupyter_notebook_config.py

RUN conda update -n base conda
RUN conda config --env --add pinned_packages 'openjdk>8.0.121'

ENV CONDA_PACKAGES="\
    jupyter_contrib_nbextensions \
    jupyter_nbextensions_configurator \
    scikit-learn \
    yapf \
    tqdm \
    qgrid \
    opencv \
    beakerx \
    "

RUN conda install --yes -c conda-forge $CONDA_PACKAGES
RUN beakerx install


COPY ./DockerConfigurations/notebook.json /home/jovyan/.jupyter/nbconfig/notebook.json
COPY ./DockerConfigurations/tree.json /home/jovyan/.jupyter/nbconfig/tree.json


WORKDIR /workdir
RUN mkdir -p /home/jovyan/.jupyter/custom
COPY ./DockerConfigurations/custom.js /home/jovyan/.jupyter/custom/custom.js

