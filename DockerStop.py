import docker

if __name__ == '__main__':
    client = docker.from_env()
    containers = client.containers.list()
    for cont in containers:
        if cont.attrs['Config']['Labels']['MAINTAINER'] == 'Maydan Wienreb <maydanw@gmail.com>':
            cont.stop()
