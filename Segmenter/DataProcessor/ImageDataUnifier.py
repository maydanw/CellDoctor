import os
from glob import glob
import re
import pandas as pd


class ImageDataUnifier:
    unification_protocol_template = [('base', {"ch_type": "Nucli"}),
                                     ('join', {"ch_type": "Cyto", "full_tbl_fld": "cyto_label"}),
                                     ('join_1_agg', {"ch_type": "MitoTracker", "full_tbl_fld": "cyto_label",
                                                     "largest_key": 'MitoTracker_size'}),
                                     ('join_1_agg', {"ch_type": "TMRE", "full_tbl_fld": "cyto_label",
                                                     "largest_key": 'TMRE_size'})
                                     ]

    def __init__(self, folder_path: str = None, save_results=True, filename="agg_results.csv",
                 unification_protocol=None):
        self.unification_protocol = unification_protocol
        if self.unification_protocol is None:
            self.unification_protocol = self.unification_protocol_template
        if folder_path is None:
            raise RuntimeError("Path of data must be specified")

        if os.path.exists(folder_path):
            self.folder_path = folder_path
        else:
            raise RuntimeError(f"Could not find path: {folder_path}")
        if folder_path[-1] not in ['/', '//']:
            folder_path = folder_path + '/'
        files = glob(folder_path + "*.csv")
        files_df = self.extract_files_data(files)
        grp = files_df.groupby(by=["row", "column", "field"])
        self.data = pd.DataFrame()
        for img_k in grp.groups:
            img_data = pd.DataFrame()
            img_df = grp.get_group(img_k)
            for action, params in self.unification_protocol:
                if type(action) is str:
                    action = getattr(self, action)
                img_data = action(img_data, img_df, **params)
            img_data["row"] = img_k[0]
            img_data["column"] = img_k[1]
            img_data["field"] = img_k[2]
            self.data = self.data.append(img_data)
        self.data = self.data.reset_index()
        if save_results:
            self.data.to_csv(folder_path + filename)

    def extract_files_data(self, files):
        p = re.compile("(.*) - (.*)\(fld (.*) wv (.*)\)-(.*).*.csv")
        tuple_list = []
        index = []
        for fn in files:
            filename = os.path.basename(fn)
            m = p.match(filename)
            if m is None:
                print(f"filename: {filename} could not be decoded by pattern {p.pattern}")
                continue
            pattern_res = m.groups()
            if len(pattern_res) != 5:
                print(f"filename: {fn} decoding does not fit expectation of 4 arguments")
                continue
            tuple_list.append(pattern_res)
            index.append(fn)
        df = pd.DataFrame.from_records(tuple_list, columns=["row", "column", "field", "channel", "ch_type"],
                                       index=index)
        return df

    def base(self, full_tbl, img_df, **kwargs):
        new_df = pd.read_csv(img_df[img_df.ch_type == kwargs["ch_type"]].index[0], index_col=0)
        return new_df

    def read_csv(self, img_df, ch_type):
        new_df = pd.DataFrame()
        try:
            fn = img_df[img_df.ch_type == ch_type]
            if fn.shape[0] == 1:
                fn = fn.index[0]
                new_df = pd.read_csv(fn, index_col=0)
            else:
                print(f"Channel {ch_type} was not found as csv and was Skipped!")
        except Exception as err:
            print(f"Reading file: {fn} encountered and error.")
            print(err)
        return new_df

    def join(self, full_tbl, img_df, **kwargs):
        new_df = self.read_csv(img_df, kwargs["ch_type"])
        if new_df.shape[0]:
            full_tbl = full_tbl.join(other=new_df, on=kwargs["full_tbl_fld"], how="left")
        return full_tbl

    def join_agg(self, full_tbl, img_df, **kwargs):
        new_df = self.read_csv(img_df, kwargs["ch_type"])
        if kwargs["full_tbl_fld"] not in new_df.columns:
            return full_tbl
        final_new_df = pd.DataFrame()
        grouper = new_df.groupby(by=[kwargs["full_tbl_fld"]])
        for grp_k in grouper.groups:
            row = pd.Series()
            grp_df = grouper.get_group(grp_k)
            grp_df = grp_df.drop(kwargs["full_tbl_fld"], axis=1)

            if grp_df.shape[0] > 0:
                row[grp_df.columns[0].split('_')[0] + '_count'] = grp_df.shape[0]
                row = row.append(grp_df.sum(axis=0).add_suffix('_sum'))
                row = row.append(grp_df.median(axis=0).add_suffix('_median'))
                row = row.append(grp_df.mean(axis=0).add_suffix('_mean'))
                row = row.append(grp_df.std(axis=0).add_suffix('_std'))
                row = row.append(grp_df.quantile(q=0.1, axis=0).add_suffix('_q10'))
                row = row.append(grp_df.quantile(q=0.9, axis=0).add_suffix('_q90'))
            final_new_df[grp_k] = row

        full_tbl = full_tbl.join(other=final_new_df.T, on=kwargs["full_tbl_fld"], how="left")
        return full_tbl

    def join_1_agg(self, full_tbl, img_df, **kwargs):
        largest_key = kwargs["largest_key"]
        new_df = self.read_csv(img_df, kwargs["ch_type"])
        if kwargs["full_tbl_fld"] not in new_df.columns:
            return full_tbl
        final_new_df = pd.DataFrame()
        grouper = new_df.groupby(by=[kwargs["full_tbl_fld"]])
        for grp_k in grouper.groups:
            row = pd.Series()
            grp_df = grouper.get_group(grp_k)
            grp_df = grp_df.drop(kwargs["full_tbl_fld"], axis=1)

            # taking the largest obj
            if grp_df.shape[0] > 0:
                largest_obj = grp_df[largest_key].idxmax()
                row = row.append(grp_df.loc[largest_obj, :].add_suffix('_1'))
            if grp_df.shape[0] > 0:
                row[grp_df.columns[0].split('_')[0] + '_count'] = grp_df.shape[0]
                row = row.append(grp_df.sum(axis=0).add_suffix('_sum'))
                row = row.append(grp_df.median(axis=0).add_suffix('_median'))
                row = row.append(grp_df.mean(axis=0).add_suffix('_mean'))
                row = row.append(grp_df.std(axis=0).add_suffix('_std'))
                row = row.append(grp_df.quantile(q=0.1, axis=0).add_suffix('_q10'))
                row = row.append(grp_df.quantile(q=0.9, axis=0).add_suffix('_q90'))
            final_new_df[grp_k] = row

        full_tbl = full_tbl.join(other=final_new_df.T, on=kwargs["full_tbl_fld"], how="left")
        return full_tbl
