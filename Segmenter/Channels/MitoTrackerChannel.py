from Segmenter.Channels.ImageChannelBase import ImageChannelBase
from typing import Dict, Type
import numpy as np
import cv2

from Segmenter.Constants import MASK_THRESHOLD


class MitoTrackerChannel(ImageChannelBase):

    name = 'MitoTracker'

    def __init__(self, path: str, dependencies: Dict[str, Type[ImageChannelBase]] = None, auto_load: bool = True):
        super().__init__(path, auto_load=auto_load)
        self.dependencies["Cyto"] = None
        self.dependencies["Nucli"] = None
        if dependencies:
            self.dependencies.update(dependencies)

    def clean_dye_leakage(self, base_img, nucli_ch):
        centers = []
        nuc_segments = nucli_ch.segments
        for idx in np.unique(nuc_segments):
            if idx == 0:
                continue
            if nucli_ch.data.nuc_outlier[idx]==True:
                continue
            v = base_img[nuc_segments == idx]
            if v.shape[0] > 0:
                centers.append(np.percentile(v, 30))
        if len(centers) > 0:
            centers_mean = np.percentile(centers, 75)
            base_img[base_img <= round(centers_mean)] = 0

    def generate_segmentation(self):
        cyto_segments = self.dependencies["Cyto"].segments
        cyto_data = self.dependencies["Cyto"].data

        # Create mito segmentation by cell segmentation
        img = self.base_image.copy()
        self.clean_dye_leakage(img, self.dependencies["Nucli"])

        counter = 1
        mito_segments = np.zeros_like(cyto_segments)
        for cyto_ind in np.unique(cyto_segments):
            if cyto_ind == 0:
                continue
            if cyto_ind not in cyto_data.index:
                continue
            if cyto_data.cyto_outlier[cyto_ind]:
                continue
            if cyto_data.Cyto_border_case[cyto_ind]:
                continue

            temp = np.zeros_like(cyto_segments, dtype=np.uint8)
            temp[cyto_segments == cyto_ind] = img[cyto_segments == cyto_ind]

            mito_markers_num, mito_segments_temp = cv2.connectedComponents(temp)
            for idx in np.unique(mito_segments_temp):
                if idx == 0:
                    continue
                mito_min_size = 6
                if mito_segments_temp[mito_segments_temp == idx].shape[0] <= mito_min_size:
                    mito_segments_temp[mito_segments_temp == idx] = 0
            mito_segments_temp[mito_segments_temp > 0] = mito_segments_temp[mito_segments_temp > 0] + counter

            mito_segments = mito_segments + mito_segments_temp

            for idx in np.unique(mito_segments_temp):
                if idx == 0:
                    continue
                self.data.loc[idx, "cyto_label"] = cyto_ind
            counter = counter + mito_markers_num

        self.segments = mito_segments
