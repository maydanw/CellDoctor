from abc import ABC, abstractmethod

import cv2
import pandas as pd
import numpy as np

from Segmenter.ImageTools.GeneralImageTools import show_image


class ImageChannelBase(ABC):
    name = 'AbstractChannel'

    def __init__(self, path: str, auto_load: bool = True, data_extractors=None):
        self.auto_load = auto_load
        self.path = path
        self.base_image = None
        self.segments = None
        self.dependencies = {}
        self.data = pd.DataFrame()
        self.data_extractors = []
        if self.data_extractors:
            self.data_extractors = data_extractors
        if auto_load:
            self.load_image()

    def load_image(self):
        self.base_image = cv2.imread(self.path, cv2.IMREAD_UNCHANGED)
        if self.base_image is None:
            raise RuntimeError(f"Could not load image: {self.path}")
        self.base_image = cv2.convertScaleAbs(self.base_image, alpha=(255.0 / np.iinfo(self.base_image.dtype).max))

    def show_image(self, img=None, show_axis='off', size=(15, 15), cmap='gray'):
        if img is None:
            img = self.base_image
        title = self.name + ' -  ' + self.path
        show_image(img, title, show_axis, size, cmap)

    def set_dependencies(self, channel):
        self.dependencies[channel.name] = channel.segments

    @abstractmethod
    def generate_segmentation(self):
        raise NotImplementedError

    # def post_init(self, ci, **kwargs): #!QAZ@WSX
    #     pass
    #
    # def add_data_extractor(self, data_extractor):
    #     self.data_extractors.append(data_extractor)

    def save_data(self, filename):
        self.data.to_csv(filename)
