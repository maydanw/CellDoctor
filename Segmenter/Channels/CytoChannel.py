import numpy as np
from typing import Dict, Type
from Segmenter.Channels.ImageChannelBase import ImageChannelBase
from Segmenter.ImageTools.SegmentationTools import build_segmentation


class CytoChannel(ImageChannelBase):
    name = 'Cyto'

    def __init__(self, path: str, dependencies: Dict[str, Type[ImageChannelBase]] = None, auto_load: bool = True):
        super().__init__(path, auto_load=auto_load)
        self.dependencies["Nucli"] = None
        if dependencies:
            self.dependencies.update(dependencies)

    def generate_segmentation(self, segmentation_resolution=1):
        if not self.validate_dependencies():
            raise RuntimeError("Channel segmentation dependencies were not set")
        # building the channels for convenience
        nucli_channel = self.dependencies["Nucli"]
        cyto_channel_img = self.base_image
        cyto_data = self.data
        if nucli_channel.data.shape[0] > 0:
            new_markers = build_segmentation(cyto_channel_img, cyto_data, nucli_channel, segmentation_resolution)
            self.segments = new_markers
        else:
            self.segments = np.zeros_like(cyto_channel_img, dtype=np.uint8)

    def validate_dependencies(self):
        for k, v in self.dependencies.items():
            if v is None:
                return False
        return True
