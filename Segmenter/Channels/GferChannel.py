from Segmenter.Channels.ImageChannelBase import ImageChannelBase
from typing import Dict, Type

from Segmenter.Constants import MASK_THRESHOLD
import numpy as np


class GferChannel(ImageChannelBase):

    name = 'GFER'

    def __init__(self, path: str, dependencies: Dict[str, Type[ImageChannelBase]] = None, auto_load: bool = True):
        super().__init__(path, auto_load=auto_load)
        self.dependencies["Cyto"] = None
        if dependencies:
            self.dependencies.update(dependencies)

    def generate_segmentation(self):
        cyto_segments = self.dependencies["Cyto"].segments
        cyto_data = self.dependencies["Cyto"].data

        # Create segmentation by cell segmentation
        img = self.base_image.copy()
        segments = np.zeros_like(cyto_segments)
        for cyto_ind in np.unique(cyto_segments):
            if cyto_ind == 0:
                continue
            if cyto_ind not in cyto_data.index:
                continue
            if cyto_data.cyto_outlier[cyto_ind]:
                continue
            if cyto_data.Cyto_border_case[cyto_ind]:
                continue
            segments[(cyto_segments == cyto_ind) & (img > MASK_THRESHOLD)] = cyto_ind

        self.segments = segments
