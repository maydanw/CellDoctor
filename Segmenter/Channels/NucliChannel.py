from Segmenter.Channels.ImageChannelBase import ImageChannelBase
import numpy as np
import cv2

from Segmenter.Constants import MASK_THRESHOLD
from Segmenter.ImageTools.GeneralImageTools import rescale


class NucliChannel(ImageChannelBase):
    name = 'Nucli'

    def __init__(self, path: str, auto_load: bool = True):
        super().__init__(path, auto_load=auto_load)

    # def post_init(self, ci, **kwargs): #!QAZ@WSX
    #
    #     self.add_data_extractor()
    #     pass

    def generate_segmentation(self):
        self.segments = self.connectedComponents_segmentation()

    def connectedComponents_segmentation(self):
        kernel3 = np.ones((3, 3), np.uint8)
        img = self.base_image.copy()
        img = rescale(img)
        _, mask = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel3, iterations=1)

        _, ret_mask = cv2.connectedComponents(mask)

        segments = np.zeros_like(ret_mask)
        counter = 1
        for ind in np.unique(ret_mask):
            if ind == 0:
                continue
            tmp_img = np.zeros_like(ret_mask, np.uint8)
            tmp_img[ret_mask == ind] = 255
            tmp_img = cv2.morphologyEx(tmp_img, cv2.MORPH_CLOSE, kernel3, iterations=3)
            tmp_img = cv2.morphologyEx(tmp_img, cv2.MORPH_OPEN, kernel3, iterations=3)
            nuc_size = np.size(tmp_img[tmp_img == 255])
            if nuc_size < 500 or nuc_size > 5000:
                continue
            segments[tmp_img == 255] = counter
            counter = counter + 1

        return segments
