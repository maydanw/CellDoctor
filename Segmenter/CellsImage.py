from typing import Dict, Type, Union, List
from Segmenter.Channels.ImageChannelBase import ImageChannelBase
import os

from Segmenter.Constants import default_channel_map
from Segmenter.ImageTools.SegmentationTools import load_class


class CellsImage(object):
    def __init__(self, image_path: str,
                 channels_types: Dict[str, Union[Type[ImageChannelBase], List[Type[ImageChannelBase]]]]):
        self.channels_names = channels_types
        if image_path[-1] not in ['/', '//']:
            image_path = image_path + '/'
        self.image_path = image_path
        self.channels: Dict[str, Type[ImageChannelBase]] = {}

        for k, v in channels_types.items():
            if type(v) is not list:
                v = [v]
            for vi in v:
                self.load_channel(k, vi)

        for k, v in self.channels.items():
            for kd, vd in v.dependencies.items():
                if vd is None and kd in self.channels:
                    v.dependencies[kd] = self.channels[kd]

    def load_channel(self, k, v):
        filename = os.path.basename(k)
        channel = v(self.image_path + filename)
        self.channels[channel.name] = channel
        setattr(self, channel.name, self.channels[channel.name])

    def save_data(self, path=None):
        if path is None:
            path = self.image_path + "/results/"
        path = os.path.normpath(path)
        if path[-1] != '/':
            path = path + '/'
        if not os.path.exists(path):
            os.mkdir(path)
        for ch_name, ch in self.channels.items():
            filename = os.path.basename(ch.path)
            filename, file_extension = os.path.splitext(filename)
            ch.save_data(filename=f"{path}{filename}-{ch_name}.csv")

    def run_image_processor(self, image_processor):
        for channel in self.channels.values():
            channel.base_image = image_processor.apply(channel.base_image)

    def process_cell_image(self, data_extractors_protocol=None):
        if not data_extractors_protocol:
            data_extractors_protocol = default_channel_map
        done = set()
        for extractor_template in data_extractors_protocol:
            trigger_after = extractor_template["trigger_after"]
            if trigger_after.endswith('Channel'):
                trigger_after = trigger_after.replace('Channel', '')
            if trigger_after not in done:
                self.channels[trigger_after].generate_segmentation()
                done.add(trigger_after)
            extractor = load_class(extractor_template["class"])
            data_sources = []
            for channel in extractor_template["data_sources"]:
                if channel.endswith('Channel'):
                    channel = channel.replace('Channel', '')
                if channel in self.channels:
                    data_sources.append(self.channels[channel])
                else:
                    data_sources.append(channel)

            extractor(*data_sources).do()
        self.save_data()
