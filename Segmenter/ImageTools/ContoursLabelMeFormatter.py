from matplotlib import cm
import numpy as np
from pathlib import Path


def convert_opencv_to_labelme(contours_dict, filename, final_file=None):
    reduce_point_with_small_distance = 6
    res_json = {}
    res_json["imagePath"] = Path(filename).name
    res_json["imageData"] = None
    res_json["lineColor"] = [0, 255, 0, 128]
    res_json["fillColor"] = [255, 0, 0, 128]
    res_json["shapes"] = []
    counter = 0
    for ind, cnt in contours_dict.items():
        counter = counter + 1
        region = {}
        region["fill_color"] = [int(i * 255) for i in cm.nipy_spectral((counter + 1) / len(contours_dict))]
        region["line_color"] = None
        region["shape_type"] = "polygon"
        points = cnt[:, 0, :]

        dist = points[:-2:2] - points[2::2]
        dist = (dist ** 2).sum(axis=1) ** 0.5
        points_index = np.argwhere(dist > reduce_point_with_small_distance)
        points_index = points_index * 2
        points_index = np.append(points_index.flatten(), points_index.flatten() + 1)
        points_index.sort()
        reduced_points = points[points_index]
        reduced_points = np.insert(reduced_points, reduced_points.shape[0], points[-1], axis=0)

        region["points"] = reduced_points.tolist()
        region["label"] = "cell_" + str(ind)
        res_json["shapes"].append(region)

    return res_json
