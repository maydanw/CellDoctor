import cv2
import math
import numpy as np


class LenCleaner:
    def __init__(self, img, step_size=512, percentile=20, params=None):
        self.step_size = step_size
        self.percentile = percentile
        if params is None:
            self.params = {}
        else:
            self.params = params

    def apply(self, img):
        ret_img = img.copy()
        signal_p10_vals = img[img > 0]
        if signal_p10_vals.shape[0] > 0:
            signal_p10 = np.percentile(img[img > 0], 10)
            ret_img[ret_img <= signal_p10] = 0

        check = np.percentile(ret_img, [10, 40])
        if check[0] != check[1]:
            x = np.arange(5, 60)
            y = np.percentile(ret_img, x)
            fit_func = np.poly1d(np.polyfit(x, y, 1))
            fit_100 = min(round(fit_func(100)), np.percentile(img[img > 0], 95))
            ret_img[ret_img <= fit_100] = 0

        blured_img = cv2.blur(ret_img, (9, 9))
        image_center = blured_img.shape[0] // 2
        mask1 = np.zeros_like(blured_img)
        cv2.circle(mask1, (image_center, image_center), self.step_size, 255, -1)
        values1 = blured_img[mask1 > 0]

        mask2 = np.zeros_like(blured_img)
        cv2.circle(mask2, (image_center, image_center), self.step_size * 2, 255, -1)
        cv2.circle(mask2, (image_center, image_center), self.step_size, 0, -1)
        values2 = blured_img[mask2 > 0]

        mask3 = np.zeros_like(blured_img)
        cv2.circle(mask3, (image_center, image_center), self.step_size * 3, 255, -1)
        cv2.circle(mask3, (image_center, image_center), self.step_size * 2, 0, -1)
        values3 = blured_img[mask3 > 0]

        p_inner = 0
        if values1.shape[0] > 0:
            p_inner = np.percentile(values1, 50)
        p_mid = 0
        if values2.shape[0] > 0:
            p_mid = np.percentile(values2, 50)
        p_outer = 0
        if values3.shape[0] > 0:
            p_outer = np.percentile(values3, 50)
        if p_inner >= p_mid >= p_outer and np.percentile(values1, 10) > 0:
            ret_img[ret_img <= np.percentile(ret_img[ret_img > 0], 10)] = 0

        # threshold, binary_image = cv2.threshold(ret_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        # ret_img[ret_img <= threshold] = 0
        return ret_img
