import json
import pandas as pd
import numpy as np

def convert_opencv_to_viaJson(contours_dict, filename, final_file=None):
    res_json = {}
    res_json["filename"] = filename
    res_json["file_attributes"] = {}
    res_json["size"] = -1
    res_json["regions"] = []
    for ind, cnt in contours_dict.items():
        region = {}
        region["region_attributes"] = {}
        all_points_x = cnt[:, 0, 0]
        all_points_y = cnt[:, 0, 1]
        region["shape_attributes"] = {"name": "polygon",
                                      "all_points_x": all_points_x.tolist(),
                                      "all_points_y": all_points_y.tolist()}

        res_json["regions"].append(region)
        res_json["name"] = str(ind)
    if final_file is None:
        final_file = dict()
    final_file["A - 02(fld 01 wv Cy3 - Cy3).tiff"] = res_json
    return final_file


def convert_opencv_to_viaCsv(contours_dict, filename, df=None):
    if df is None:
        df = pd.DataFrame()
    region_count = len(contours_dict)
    i = df.shape[0]
    for ind, cnt in contours_dict.items():
        df.loc[ind, "filename"] = filename
        df.loc[ind, "file_size"] = -1
        df.loc[ind, "file_attributes"] = "{}"
        df.loc[ind, "region_count"] = region_count
        df.loc[ind, "region_id"] = i

        all_points_x = cnt[:, 0, 0]
        all_points_y = cnt[:, 0, 1]
        region = {"name": "polygon",
                  "all_points_x": all_points_x.tolist(),
                  "all_points_y": all_points_y.tolist()}
        region_str = str(region)
        df.loc[ind, "region_shape_attributes"] = region_str
        df.loc[ind, "region_attributes"] = "{}"

        i = i + 1

    df["file_size"] = df["file_size"].astype(np.int)
    df["region_count"] = df["region_count"].astype(np.int)
    df["region_id"] = df["region_id"].astype(np.int)
    return df
