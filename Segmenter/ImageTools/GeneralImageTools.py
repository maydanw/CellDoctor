import numpy as np
import cv2
import matplotlib.pyplot as plt


def rescale(img):
    pixels_values = img[img > 0]
    if pixels_values.shape[0] == 0:
        return img

    upper = np.percentile(pixels_values, 99)
    lower = np.amin(img)

    if upper == lower:
        upper = np.percentile(pixels_values, 99.9)
        if upper == lower:
            return img

    ret_img = img.copy()
    ret_img[ret_img > upper] = upper
    ret_img = 255 * ((img - lower) / (upper - lower))
    ret_img = ret_img.astype(np.uint8)
    return ret_img


def reduce_background(img):
    pixels_values = img[img > 0]
    if pixels_values.shape[0] == 0:
        return img
    background_val = np.percentile(pixels_values, q=10)
    ret_img = img.copy()
    ret_img[ret_img <= background_val] = 0
    # ret_img = cv2.fastNlMeansDenoising(ret_img)
    return ret_img


def show_image(img, title="", show_axis='off', size=(15, 15), cmap='nipy_spectral'):
    fig, axis = plt.subplots(1, 1, figsize=size)
    plt.title(title)
    axis.axis(show_axis)
    plt.imshow(img, cmap=cmap)
    plt.show()


def image_percentile_plot(img, size=(15, 15)):
    percentiles = np.arange(100) + 1
    p_vals = np.percentile(img, percentiles)
    fig, axis = plt.subplots(1, 1, figsize=size)
    plt.plot(percentiles, p_vals)
    plt.show()


def save_image(img, filename):
    cv2.imwrite(filename, img)
