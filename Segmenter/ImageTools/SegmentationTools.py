import cv2
import numpy as np
from Segmenter.Constants import MASK_THRESHOLD
from pydoc import locate

from Segmenter.ImageTools.GeneralImageTools import rescale


def build_segmentation(cyto_channel_img, cyto_data, nucli_channel, segmentation_resolution):
    if nucli_channel.data.shape[0] == 0:
        return np.zeros_like(cyto_channel_img)
    kernel = np.ones((3, 3), np.uint8)
    cyto_mask = np.zeros_like(cyto_channel_img, dtype=np.uint8)
    cyto_mask[cyto_channel_img > MASK_THRESHOLD] = 255
    nuc_mask = np.zeros_like(nucli_channel.segments, dtype=np.uint8)
    nuc_mask[nucli_channel.segments > 0] = 255
    nucli_data = nucli_channel.data
    if 'nuc_outlier' not in nucli_data.columns:
        nucli_data['nuc_outlier'] = False
    for i in nucli_data.loc[nucli_data.nuc_outlier].index:
        nuc_mask[nucli_channel.segments == i] = 0
        cyto_mask[nucli_channel.segments == i] = 0
    cyto_mask = cv2.morphologyEx(cyto_mask, cv2.MORPH_OPEN, kernel, iterations=2)
    markers = calc_watershed(cyto_channel_img, cyto_mask, kernel, nuc_mask, segmentation_resolution)
    # Prepare the data for further steps
    markers[markers == -1] = 0
    markers[markers == 1] = 0
    connect_cyto_nucli_labels(markers, cyto_data, nucli_channel)
    markers_to_ignore = set()
    markers_to_constraint = get_outliers_markers(markers, nucli_channel)
    markers, markers_to_ignore = clean_cyto_labels_without_nucli(markers, nucli_channel, markers_to_ignore,
                                                                 markers_to_constraint)
    # TODO: To check maybe clean_small_markers can be removed
    markers, markers_to_ignore = clean_small_markers(markers, markers_to_constraint, markers_to_ignore,
                                                     nucli_channel)
    new_markers = expend_seeds(cyto_mask, kernel, markers, markers_to_constraint, markers_to_ignore, cyto_channel_img)
    return new_markers


def connect_cyto_nucli_labels(markers, cyto_data, nucli_channel):
    # add the reference between the nucli segmentation and the forming cyto segmentation
    nucli_channel.data["cyto_label"] = None
    for i, row in nucli_channel.data.loc[:, ['Nucli_morphological_cx', 'Nucli_morphological_cy', 'nuc_outlier']].astype(
            np.int).iterrows():
        if row.nuc_outlier:
            continue
        cyto_label = markers[row.Nucli_morphological_cy, row.Nucli_morphological_cx]
        nucli_channel.data.loc[i, "cyto_label"] = cyto_label
        cyto_data.loc[cyto_label, "nucli_label"] = i


def expend_seeds(cyto_mask, kernel, markers, markers_to_constraint, markers_to_ignore, cyto_channel_img):
    # TODO: This memory consumption can be reduced by removing the cleaned arrays
    # TODO: Using gaussian blur will generate values related to the amount of surrounding black. Using this information will generate better overlaps
    num_cyto = markers.max()
    all_cytos = np.zeros((markers.shape[0], markers.shape[1], num_cyto + 1))

    cyto_mask_zeros = cyto_mask == 0
    new_markers = markers.copy()
    for ind in np.unique(markers):
        if ind == 0:
            continue
        all_cytos[markers == ind, ind] = ind
    iter_num = 30  # TODO: Replace magic number with minimum cyto coverage
    expansion_rate = 5  # TODO: Remove the magic number
    done_segmentataion = []
    print_trh = [0, 0.25, 0.5, 0.75, 1.0]
    print_trh_counter = 0
    for iter_ind in range(iter_num):
        if (iter_ind + 1) / float(iter_num) >= print_trh[print_trh_counter]:
            print(f"Started processing iteration: {iter_ind + 1} out of {iter_num}")
            print_trh_counter = print_trh_counter + 1
        for ind in np.unique(markers):
            if ind == 0:
                continue
            if ind in markers_to_ignore:
                continue
            # While this if can be conmbined with above they are logically different and i prefrer to keep them seperated for readablity
            if ind in markers_to_constraint:
                continue
            if ind in done_segmentataion:
                continue
            temp = all_cytos[:, :, ind]
            temp = cv2.dilate(temp, kernel, iterations=expansion_rate + iter_ind // 3)
            temp[cyto_mask_zeros] = 0
            overlap_mask = np.bitwise_and(new_markers > 0, new_markers != temp)
            temp[overlap_mask] = 0
            if (temp == all_cytos[:, :, ind]).all():
                done_segmentataion.append(ind)
            all_cytos[:, :, ind] = temp

        new_markers = all_cytos.argmax(axis=2)

        # all_cytos_view = all_cytos.copy()
        # all_cytos_view[all_cytos_view > 0] = 1
        # collisions = np.where(all_cytos_view.sum(axis=2) > 1)
        #
        # all_cytos_conflict_resolver = cyto_channel_img.reshape((2048,2048,1)).copy()
        # all_cytos_conflict_resolver = all_cytos_conflict_resolver*all_cytos_view
        #
        # krnl = np.ones((9, 9), np.float32) / 81
        # all_cytos_conflict_resolver = cv2.filter2D(all_cytos_conflict_resolver[:, :, :], -1, krnl)
        #
        # for y, x in zip(collisions[0], collisions[1]):
        #     new_markers[y, x] = all_cytos_conflict_resolver[y, x, :].argmax()

        new_markers[cyto_mask_zeros] = 0
    return new_markers


def calc_watershed(cyto_channel_img, cyto_mask, kernel, nuc_mask, segmentation_resolution):
    # dist_transform = cv2.distanceTransform(nuc_mask, cv2.DIST_L2, 5) + nuc_mask * 5 + \
    #                  cv2.distanceTransform(cyto_mask, cv2.DIST_L2, 3) * 0.5
    dist_transform = cv2.distanceTransform(nuc_mask, cv2.DIST_L2, 5) + nuc_mask * 5
    dist_transform = rescale(dist_transform)
    ret, sure_fg = cv2.threshold(dist_transform, segmentation_resolution, 255, 0)
    sure_fg = sure_fg.astype(np.uint8)
    sure_fg = cv2.morphologyEx(sure_fg, cv2.MORPH_OPEN, kernel, iterations=5)
    # Finding unknown region
    unknown = cv2.subtract(cyto_mask, sure_fg)
    ret, markers = cv2.connectedComponents(sure_fg)
    # Add one to all labels so that sure background is not 0, but 1
    markers = markers + 1
    # Now, mark the region of unknown with zero to fill only them in the following watershed
    markers[unknown == 255] = 0
    zipped = np.dstack((cyto_channel_img, cyto_channel_img, cyto_channel_img))
    markers = cv2.watershed(zipped, markers)
    return markers


def clean_cyto_labels_without_nucli(markers, nucli_channel, markers_to_ignore, markers_to_constraint):
    all_cyto_labels = set(np.unique(markers))
    all_cyto_labels.discard(0)
    cyto_labels_with_no_nucli = all_cyto_labels - set(
        nucli_channel.data.cyto_label[nucli_channel.data.cyto_label > 0].tolist())

    for ind in np.unique(markers):
        if ind == 0 or ind in markers_to_constraint:
            continue
        if ind in cyto_labels_with_no_nucli:
            markers[markers == ind] = 0
            markers_to_ignore.add(ind)
    return markers, markers_to_ignore


def clean_small_markers(markers, markers_to_constraint, markers_to_ignore, nucli_channel):
    min_size_nuc = nucli_channel.data[nucli_channel.data.nuc_outlier == False].loc[:, "Nucli_intensity_count"].min()

    for ind in np.unique(markers):
        if ind == 0 or ind in markers_to_constraint:
            continue
        if markers[markers == ind].shape[0] < min_size_nuc * 0.9:
            markers_to_ignore.add(ind)
            markers[markers == ind] = 0
    return markers, markers_to_ignore


def get_outliers_markers(markers, nucli_channel):
    # TODO: This method may be redundant as we clean the mask beforehand
    markers_to_constraint = []
    cx = nucli_channel.name + "_morphological_" + 'cx'
    cy = nucli_channel.name + "_morphological_" + 'cy'
    if cx not in nucli_channel.data.columns or cy not in nucli_channel.data.columns:
        return markers_to_constraint
    for i, row in nucli_channel.data.loc[nucli_channel.data.nuc_outlier == True, [cx, cy]].astype(
            np.int).iterrows():
        outlier_marker = markers[row[cy], row[cx]]
        if outlier_marker > 0:
            markers_to_constraint.append(outlier_marker)
    return markers_to_constraint


def load_class(class_type):
    cls = locate(class_type)
    if cls is None:
        cls = locate(f"Segmenter.Channels.{class_type}.{class_type}")
    if cls is None:
        cls = locate(f"Segmenter.DataExtractors.{class_type}.{class_type}")

    if cls is None:
        raise (f"Could not load protocol. Channel {class_type} could not be located.")
    return cls
