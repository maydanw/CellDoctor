# Changes made on LabelMe code
1. conda install qimage2ndarray
2. At app.py added:  
```python
        a = qimage2ndarray.rgb_view(image)
        a = ((a*3) * (255 / a.max() - a.min()))
        a = np.clip(a, 0, 255).astype(np.uint8)
        image = qimage2ndarray.array2qimage(a, normalize=True)
``` 
3. In saveLabels I remarked ```imageData=imageData``` in order to keep the files size small