from typing import Type
from pathlib import Path

import numpy as np
import cv2

from Segmenter.Channels.ImageChannelBase import ImageChannelBase
from Segmenter.DataExtractors.DataExtractorBase import DataExtractorBase


class ImageMergeExporter(DataExtractorBase):
    """
    Using Actin as Cyto by blurring the Actin channel and and closing holes between the lines
    """

    def __init__(self, base_channel: Type[ImageChannelBase], *args):
        super().__init__(base_channel)
        if len(args) > 2:
            raise RuntimeError("Only 2 channels are allowed in addition to the base channel")
        self.additional_channels = args
        self.base_channel_filename = self.base_channel.path

    def do(self):
        images = [self.base_channel.base_image]
        for channel in self.additional_channels:
            images.append(channel.base_image)
        for i in range(3 - len(images)):
            images.append(np.zeros_like(self.base_channel.base_image))
        unified_image = np.dstack(images)
        filename = '_'.join(Path(self.base_channel_filename).parts[-1:])
        filename = filename.replace('tif', 'jpg')
        file_path = Path(self.base_channel_filename).parent.joinpath('results', filename)
        cv2.imwrite(str(file_path), cv2.cvtColor(unified_image, cv2.COLOR_BGR2RGB))
