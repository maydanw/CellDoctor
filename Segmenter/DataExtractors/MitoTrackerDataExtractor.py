from typing import Type

from Segmenter.Channels.MitoTrackerChannel import MitoTrackerChannel
from Segmenter.DataExtractors.DataExtractorBase import DataExtractorBase, intensity_measurements, \
    morphological_measurements
import numpy as np


class MitoTrackerDataExtractor(DataExtractorBase):
    def __init__(self, mito_channel: Type[MitoTrackerChannel]):
        super().__init__(mito_channel)

    def do(self):
        df = self.base_channel.data
        for idx in np.unique(self.base_channel.segments):
            if idx == 0:
                continue

            intensity_measurements(df, idx, self.base_channel.base_image, self.base_channel.segments,
                                   self.base_channel.name + "_")
            morphological_measurements(df, idx, self.base_channel.segments, self.base_channel.name + "_")
