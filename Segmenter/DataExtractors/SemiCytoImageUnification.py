from typing import Type

import numpy as np
import cv2

from Segmenter.Channels.ImageChannelBase import ImageChannelBase
from Segmenter.DataExtractors.DataExtractorBase import DataExtractorBase


class SemiCytoImageUnification(DataExtractorBase):
    """
    Using Actin as Cyto by blurring the Actin channel and and closing holes between the lines
    """

    def __init__(self, base_channel: Type[ImageChannelBase], *args):
        super().__init__(base_channel)
        self.additional_channels = args

    def do(self):
        channels_base_images = [self.base_channel.base_image]
        mask = np.zeros(self.base_channel.base_image.shape)
        for channel in self.additional_channels:
            img = channel.base_image.copy()
            channels_base_images.append(img)
        stacked_image = np.stack(channels_base_images)
        np.amax(stacked_image, axis=0, out=mask)
        mask = cv2.morphologyEx(mask, cv2.MORPH_DILATE, kernel=(3, 3), iterations=5)
        blurred_semi_cyto_image = cv2.GaussianBlur(mask, (5, 5), 5)
        final_image = cv2.morphologyEx(blurred_semi_cyto_image, cv2.MORPH_CLOSE, kernel=(3, 3), iterations=10)
        final_image = cv2.morphologyEx(final_image, cv2.MORPH_OPEN, kernel=(3, 3), iterations=15)
        self.base_channel.base_image = final_image.astype(np.uint8)
