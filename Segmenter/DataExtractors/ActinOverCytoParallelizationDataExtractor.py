from typing import Type
import numpy as np
import cv2
from Segmenter.DataExtractors.DataExtractorBase import DataExtractorBase
from Segmenter.Channels.ActinChannel import ActinChannel
from Segmenter.Channels.SemiCytoChannel import SemiCytoChannel
from Segmenter.Channels.NucliChannel import NucliChannel


class ActinOverCytoParallelizationDataExtractor(DataExtractorBase):
    def __init__(self, actin_channel: Type[ActinChannel], cyto_channel: Type[SemiCytoChannel], nucli_channel: Type[NucliChannel]):
        super().__init__(actin_channel)
        self.cyto_channel = cyto_channel
        self.nucli_channel = nucli_channel


    def do(self):
        df = self.base_channel.data
        low_threshold = 1
        high_threshold = 25
        rho = 1  # distance resolution in pixels of the Hough grid
        theta = np.pi / 180  # angular resolution in radians of the Hough grid
        threshold = 15  # minimum number of votes (intersections in Hough grid cell)
        min_line_length = 30  # minimum number of pixels making up a line
        max_line_gap = 30  # maximum gap in pixels between connectable line segments
        actin_img = self.base_channel.base_image
        cyto_segmented_image = self.cyto_channel.segments
        for idx in np.unique(self.base_channel.segments):
            if idx == 0:
                continue
            nucli_index = self.nucli_channel.data[self.nucli_channel.data.cyto_label == idx].index
            if nucli_index.shape[0] != 1:
                continue
            nucli_index = nucli_index[0]
            mask = np.zeros(np.shape(self.base_channel.base_image), dtype=np.uint8)
            slopes_list = []
            #making sure i`m only working with a specific cyto at the time
            mask[cyto_segmented_image == nucli_index] = actin_img[cyto_segmented_image == nucli_index]
            # process edge detection use Canny
            edges = cv2.Canny(mask, low_threshold, high_threshold)
            # use HoughLinesP to get array with endpoints of actin detected
            # Output "lines" is an array containing endpoints of detected line segments
            lines = cv2.HoughLinesP(edges, rho, theta, threshold, np.array([]),
                                    min_line_length, max_line_gap)

            try:
                for line in lines:
                    for x1, y1, x2, y2 in line:
                        #i added this line so divide by zero error, won`t be a problem. my solution: adding epsilon
                        #number.
                        if x1 == x2:
                            x1 = x2 + 0.5
                        slope = (y1 - y2)/(x1 - x2)
                        slopes_list.append(slope)
            except TypeError:
                print('Couldn`t create Actin Cap over all the cytoplasms in the image')

            if len(slopes_list) == 0:
                continue
            else:
                df.loc[idx, self.base_channel.name + "_fibres" + "_slope_avg"] = np.mean(slopes_list)
                df.loc[idx, self.base_channel.name + "_fibres" + "_slope_std"] = np.std(slopes_list)
