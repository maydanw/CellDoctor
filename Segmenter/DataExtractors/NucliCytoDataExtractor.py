from typing import Type

from Segmenter.Channels.CytoChannel import CytoChannel
from Segmenter.DataExtractors.DataExtractorBase import DataExtractorBase, intensity_measurements
from Segmenter.Channels.NucliChannel import NucliChannel
import numpy as np


class NucliCytoDataExtractor(DataExtractorBase):
    """
    Looking at the Cyto channel with Nucli mask. Searching and marking outliers
    """
    def __init__(self, nucli_channel: Type[NucliChannel], cyto_channel: Type[CytoChannel]):
        super().__init__(nucli_channel)
        self.cyto_channel = cyto_channel

    def do(self, number_of_std=3):
        df = self.base_channel.data
        outliers = []

        for idx in np.unique(self.base_channel.segments):
            if idx == 0:
                continue
            intensity_measurements(df, idx, self.cyto_channel.base_image, self.base_channel.segments,
                                   prefix="nuc_in_cyto_")

            if "nuc_in_cyto_intensity_q50" in df.columns and df.loc[idx, "nuc_in_cyto_intensity_q50"] == 0:
                outliers.append(idx)

        if df.shape[0] > 10:
            c_outliers = df[(df["nuc_in_cyto_intensity_std"] - number_of_std * df["nuc_in_cyto_intensity_std"].mean()) > 0].index
            n_outliers = df[(df[self.base_channel.name + "_intensity_std"] - number_of_std * df[
                self.base_channel.name + "_intensity_std"].mean()) > 0].index
            outliers=outliers+c_outliers.append(n_outliers).tolist()

        outliers = list(dict.fromkeys(outliers))
        if 'nuc_outlier' not in df.columns:
            df['nuc_outlier'] = False
        df.loc[outliers, 'nuc_outlier'] = True
