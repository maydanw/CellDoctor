from typing import Type
import numpy as np
from Segmenter.DataExtractors.DataExtractorBase import DataExtractorBase
from Segmenter.Channels.ActinChannel import ActinChannel
from Segmenter.Channels.NucliChannel import NucliChannel



class ActinDataExtractor(DataExtractorBase):
    def __init__(self, actin_channel: Type[ActinChannel], nucli_channel: Type[NucliChannel]):
        super().__init__(actin_channel)
        self.nucli_channel = nucli_channel

    def do(self):
        df = self.base_channel.data
        nucli_segments = self.nucli_channel.segments
        actin_img = self.base_channel.base_image
        for idx in np.unique(self.base_channel.segments):
            if idx == 0:
                continue
            nucli_index = self.nucli_channel.data[self.nucli_channel.data.cyto_label == idx].index
            if nucli_index.shape[0] != 1:
                continue
            nucli_index = nucli_index[0]
            mask = np.zeros(np.shape(self.base_channel.base_image))
            pixels_values = actin_img[nucli_segments == nucli_index]
            mask[nucli_segments == nucli_index] = actin_img[nucli_segments == nucli_index]
            #mask[mask <= 1] = 0 lowers the noise of the actin img
            mask[mask <= 1] = 0
            number_of_pixels_over_nucli = np.count_nonzero(mask)

            #organazing the data in a dataframe structure
            df.loc[idx, self.base_channel.name + "_" + "number_of_actin_pixels_over_nucli"] = number_of_pixels_over_nucli
            df.loc[idx, self.base_channel.name + "_" + "intensity_std"] = np.std(pixels_values)
            df.loc[idx, self.base_channel.name + "_" + "intensity_avg"] = pixels_values.mean()
            df.loc[idx, self.base_channel.name + "_" + "q10"] = np.percentile(pixels_values, q=[10])
            df.loc[idx, self.base_channel.name + "_" + "q50"] = np.percentile(pixels_values, q=[50])
            df.loc[idx, self.base_channel.name + "_" + "q90"] = np.percentile(pixels_values, q=[90])