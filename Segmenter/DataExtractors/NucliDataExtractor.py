from typing import Type

from Segmenter.DataExtractors.DataExtractorBase import DataExtractorBase, intensity_measurements, \
    morphological_measurements
from Segmenter.Channels.NucliChannel import NucliChannel
import numpy as np


class NucliDataExtractor(DataExtractorBase):
    """
    Extract the nuclei properties (morphological and intensity)
    """

    def __init__(self, nucli_channel: Type[NucliChannel], margin_to_ignore=150, min_side=25, max_side=200):
        super().__init__(nucli_channel)
        self.min_side = min_side
        self.max_side = max_side
        self.margin_to_ignore = margin_to_ignore

    def do(self):
        outliers = set()
        df = self.base_channel.data

        for idx in np.unique(self.base_channel.segments):
            if idx == 0:
                continue
            intensity_measurements(df, idx, self.base_channel.base_image, self.base_channel.segments,
                                   self.base_channel.name + "_")

            morphological_measurements(df, idx, self.base_channel.segments, self.base_channel.name + "_")

            if df.loc[idx, self.base_channel.name + "_morphological_" + "circularity_size_ratio"] < 0.33:
                outliers.add(idx)
            if df.loc[idx, self.base_channel.name + "_morphological_" + "h"] < self.min_side or df.loc[
                idx, self.base_channel.name + "_morphological_" + "w"] < self.min_side:
                outliers.add(idx)
            if df.loc[idx, self.base_channel.name + "_morphological_" + "h"] > self.max_side or df.loc[
                idx, self.base_channel.name + "_morphological_" + "w"] > self.max_side:
                outliers.add(idx)

        if "Nucli_intensity_std" in df.columns:
            outliers = outliers.union(df[(df["Nucli_intensity_std"] - 2.5 * df["Nucli_intensity_std"].mean()) > 0].index)

        if 'nuc_outlier' not in df.columns:
            df['nuc_outlier'] = False
        df.loc[outliers, 'nuc_outlier'] = True

        if self.base_channel.name + "_" + 'border_case' not in df.columns:
            df[self.base_channel.name + "_" + 'border_case'] = False

        if df.shape[0] > 0:
            df.loc[df.Nucli_morphological_cy < self.margin_to_ignore, self.base_channel.name + "_" + 'border_case'] = True
            df.loc[df.Nucli_morphological_cx < self.margin_to_ignore, self.base_channel.name + "_" + 'border_case'] = True
            df.loc[df.Nucli_morphological_cy > self.base_channel.segments.shape[
                0] - 200, self.base_channel.name + "_" + 'border_case'] = True
            df.loc[df.Nucli_morphological_cx > self.base_channel.segments.shape[
                1] - 200, self.base_channel.name + "_" + 'border_case'] = True
