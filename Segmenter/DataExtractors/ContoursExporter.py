import json
from typing import Type

from Segmenter.DataExtractors.DataExtractorBase import DataExtractorBase
from Segmenter.Channels.CytoChannel import CytoChannel
import numpy as np
import cv2

from Segmenter.ImageTools.ContoursLabelMeFormatter import convert_opencv_to_labelme


class ContoursExporter(DataExtractorBase):
    """
    Extract the cell contours and export them to a json file (LabelMe format).
    If filename is not stated it will use a default naming
    Notes:
    * There is a reduction of segment resolution in the process to allow manual changes
    * If filename is not specified the filename will follow the cyto format

    Example:
        {...
        "DataExtractors": [
                      {"class": "SemiCytoImageUnification", "data_sources": ["Cyto", "Nucli", "Cox17", "MitoTracker"],
                       "trigger_after": "Nucli"},
                              {"class": "ContoursExporter", "data_sources": ["Cyto"],
                       "trigger_after": "Cyto"}
                       ]
        }
    """

    def __init__(self, cyto_channel: Type[CytoChannel], filename=None):
        super().__init__(cyto_channel)
        if filename:
            self.filename = filename
        else:
            self.filename = self.base_channel.path

    def do(self):

        segments_image = self.base_channel.segments
        contours_dict = {}

        for idx in np.unique(segments_image):
            if idx == 0:
                continue
            mask = np.zeros(segments_image.shape, np.uint8)
            mask[segments_image == idx] = 255
            nuc_contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            cnt = max(nuc_contours, key=cv2.contourArea)
            contours_dict[idx] = cnt

        formatted_data = convert_opencv_to_labelme(contours_dict, self.filename)
        filename = self.filename[:self.filename.rfind('.')] + '.json'
        with open(filename, 'w') as fn:
            json.dump(formatted_data, fn)
        print(f'file: {filename} was exported')
