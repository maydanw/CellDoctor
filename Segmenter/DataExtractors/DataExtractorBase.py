from abc import ABC, abstractmethod
from typing import Type
import numpy as np
import cv2

from Segmenter.Channels.ImageChannelBase import ImageChannelBase


def intensity_measurements(df, idx, base_image, segments_image, prefix=None):
    pixels_values = base_image[segments_image == idx]
    if pixels_values.shape[0] == 0:
        return
    if prefix is None:
        prefix = ""
    prefix = prefix + "intensity_"
    df.loc[idx, prefix + "count"] = pixels_values.shape[0]
    df.loc[idx, prefix + "avg"] = pixels_values.mean()
    df.loc[idx, prefix + "std"] = pixels_values.std()
    df.loc[idx, prefix + "q10"] = np.percentile(pixels_values, q=[10])
    df.loc[idx, prefix + "q50"] = np.percentile(pixels_values, q=[50])
    df.loc[idx, prefix + "q90"] = np.percentile(pixels_values, q=[90])
    df.loc[idx, prefix + "max"] = pixels_values.max()
    df.loc[idx, prefix + "sum"] = pixels_values.sum()


def morphological_measurements(df, idx, segments_image, prefix=None):
    # TODO: add morphological indicator in the prefix for better clarity in the analysis phasee.g., avg_morph
    if prefix is None:
        prefix = ""
    prefix = prefix + "morphological_"
    # Ref: https://docs.opencv.org/3.4/d1/d32/tutorial_py_contour_properties.html
    mask = np.zeros(segments_image.shape, np.uint8)
    mask[segments_image == idx] = 255
    size = mask[mask > 0].shape[0]
    nuc_contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnt = max(nuc_contours, key=cv2.contourArea)
    x, y, w, h = cv2.boundingRect(cnt)
    area = cv2.contourArea(cnt)
    rect_area = w * h
    hull = cv2.convexHull(cnt)
    hull_area = cv2.contourArea(hull)
    if hull_area != 0:
        df.loc[idx, prefix + "solidity"] = float(area) / hull_area
    if rect_area != 0:
        # Extent is the ratio of contour area to bounding rectangle area.
        df.loc[idx, prefix + "extent"] = float(area) / rect_area
    if h != 0:
        # It is the ratio of width to height of bounding rect of the object.
        df.loc[idx, prefix + "aspect_ratio"] = float(w) / h

    # compute the center of the contour
    M = cv2.moments(cnt)
    df.loc[idx, prefix + "h"] = h
    df.loc[idx, prefix + "w"] = w

    if M["m00"] != 0:
        df.loc[idx, prefix + "cx"] = int(M["m10"] / M["m00"])
        df.loc[idx, prefix + "cy"] = int(M["m01"] / M["m00"])

    df.loc[idx, prefix + "area"] = area
    df.loc[idx, prefix + "rect_area"] = rect_area
    # Solidity is the ratio of contour area to its convex hull area.
    df.loc[idx, prefix + "perimeter"] = cv2.arcLength(cnt, True)

    min_enclosing_circle_area = cv2.minEnclosingCircle(cnt)[1] ** 2 * np.pi
    df.loc[idx, prefix + "min_enclosing_circle_area"] = min_enclosing_circle_area
    circularity_size_ratio = float(size) / max(min_enclosing_circle_area, 1)
    df.loc[idx, prefix + "circularity_size_ratio"] = circularity_size_ratio


class DataExtractorBase(ABC):
    def __init__(self, base_channel: Type[ImageChannelBase]):
        self.base_channel = base_channel

    @abstractmethod
    def do(self):
        raise NotImplementedError
