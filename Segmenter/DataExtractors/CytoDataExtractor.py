from typing import Type

from Segmenter.DataExtractors.DataExtractorBase import DataExtractorBase, intensity_measurements, \
    morphological_measurements
from Segmenter.Channels.CytoChannel import CytoChannel
import numpy as np


class CytoDataExtractor(DataExtractorBase):
    def __init__(self, cyto_channel: Type[CytoChannel], min_side=75, max_side=700):
        super().__init__(cyto_channel)
        self.max_side = max_side
        self.min_side = min_side

    def do(self, min_pixels_in_border=10):
        df = self.base_channel.data
        outliers = set()

        for idx in np.unique(self.base_channel.segments):
            if idx == 0:
                continue

            intensity_measurements(df, idx, self.base_channel.base_image, self.base_channel.segments,
                                   self.base_channel.name + "_")
            morphological_measurements(df, idx, self.base_channel.segments, self.base_channel.name + "_")

            if df.loc[idx, self.base_channel.name + "_morphological_circularity_size_ratio"] > 0.95:
                outliers.add(idx)
            if df.loc[idx, self.base_channel.name + "_morphological_h"] < self.min_side or df.loc[
                idx, self.base_channel.name + "_morphological_w"] < self.min_side:
                outliers.add(idx)
            if df.loc[idx, self.base_channel.name + "_morphological_h"] > self.max_side or df.loc[
                idx, self.base_channel.name + "_morphological_w"] > self.max_side:
                outliers.add(idx)

        if 'cyto_outlier' not in df.columns:
            df['cyto_outlier'] = False
        df.loc[outliers, 'cyto_outlier'] = True

        if self.base_channel.name + "_" + 'border_case' not in df.columns:
            df[self.base_channel.name + "_" + 'border_case'] = False

        border_cases = set()
        segments = self.base_channel.segments

        items_id, freq = np.unique(segments[:1, :], return_counts=True)
        border_cases = border_cases.union(items_id[freq > min_pixels_in_border])

        items_id, freq = np.unique(segments[:, :1], return_counts=True)
        border_cases = border_cases.union(items_id[freq > min_pixels_in_border])

        items_id, freq = np.unique(segments[-1:, :], return_counts=True)
        border_cases = border_cases.union(items_id[freq > min_pixels_in_border])

        items_id, freq = np.unique(segments[:, -1:], return_counts=True)
        border_cases = border_cases.union(items_id[freq > min_pixels_in_border])

        border_cases.discard(0)
        df.loc[border_cases, self.base_channel.name + "_" + "border_case"] = True

