from typing import Type

from Segmenter.Channels.ImageChannelBase import ImageChannelBase
from Segmenter.DataExtractors.DataExtractorBase import DataExtractorBase
import numpy as np


class AntiBodyCoLocalizationDataExtractor(DataExtractorBase):
    def __init__(self, ab_channel: Type[ImageChannelBase], other_channel):
        super().__init__(ab_channel)
        self.other_channel = other_channel

    def do(self):
        for idx in np.unique(self.base_channel.segments):
            if idx == 0:
                continue

            self.intensity_measurements(idx, self.base_channel.base_image[
                (self.other_channel.segments > 0) & (self.base_channel.segments == idx)],
                                        self.base_channel.name + "_in_" + self.other_channel.name + "_")
            self.intensity_measurements(idx, self.base_channel.base_image[
                (self.other_channel.segments == 0) & (self.base_channel.segments == idx)],
                                        self.base_channel.name + "_outside_" + self.other_channel.name + "_")
            self.base_channel.data = self.base_channel.data.fillna(0)

    def intensity_measurements(self, idx, pixels_values, prefix=None):
        if pixels_values.shape[0] == 0:
            return
        if prefix is None:
            prefix = ""
        # TODO: add intensity indicator in the prefix for better clarity in the analysis phase e.g., avg_int
        df = self.base_channel.data
        df.loc[idx, prefix + "size"] = pixels_values.shape[0]
        df.loc[idx, prefix + "avg"] = pixels_values.mean()
        df.loc[idx, prefix + "std"] = pixels_values.std()
        df.loc[idx, prefix + "q10"] = np.percentile(pixels_values, q=[10])
        df.loc[idx, prefix + "q50"] = np.percentile(pixels_values, q=[50])
        df.loc[idx, prefix + "q90"] = np.percentile(pixels_values, q=[90])
        df.loc[idx, prefix + "max"] = pixels_values.max()
