# Deep Learning Segmentation considerations 
---
## Base image
There are multiple options what is the best base image and each option have it's pros and cons.

###  Cyto and Nucli image merging
\- Cyto only make things simpler - no need for special image generating in the training as well as in the system itself.  
\+ The Nucli image seems to add important information   

### With pre processing 
\+ Much of the cleaning work had been done with reduce the network task's complexity.  
\+ There is a "standardization" process of the images
\+ The current labeling is based on the cleaned image
\- Each change in the cleaning procedure will demand re-training of the network
\- Cleaning may be wrong
 
### FITCH and Actin together
\- The base image we are building on are different and have different properties
\+ A single model instead on multiple segmentation models

## Current decision
1. Different models to live and fixed cells
1. Use the merged imaged of Cyto and Nucli
1. Use the images without pre-processing