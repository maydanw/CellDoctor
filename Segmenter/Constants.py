MASK_THRESHOLD = 1

default_channel_map = {"DAPI - DAPI": "NucliChannel",
                       "FITC - FITC": "CytoChannel",
                       "Cy5 - Cy5": "MitoTrackerChannel",
                       "Cy3 - Cy3": "TMREChannel",
                       "DataExtractors": [{"class": "NucliDataExtractor", "data_sources": ["Nucli"],
                                           "trigger_after": "Nucli"},
                                          {"class": "NucliCytoDataExtractor", "data_sources": ["Nucli", "Cyto"],
                                           "trigger_after": "Nucli"},
                                          {"class": "CytoDataExtractor", "data_sources": ["Cyto"],
                                           "trigger_after": "Cyto"},
                                          {"class": "MitoTrackerDataExtractor", "data_sources": ["MitoTracker"],
                                           "trigger_after": "MitoTracker"},
                                          {"class": "TMREDataExtractor", "data_sources": ["TMRE"],
                                           "trigger_after": "TMRE"}]
                       }
