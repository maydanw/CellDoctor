from unittest import TestCase
import pandas as pd
import numpy as np

import os, sys
sys.path.append(os.getcwd()+'/../..')

from Segmenter.DataProcessor.ImageDataUnifier import ImageDataUnifier


class TestImageDataUnifier(TestCase):
    def test_extract_files_data(self):
        if os.path.exists(os.getcwd()+"/Data/DataUnifier/agg_results.csv"):
            os.remove(os.getcwd()+"/Data/DataUnifier/agg_results.csv")

        ImageDataUnifier(os.getcwd()+"/Data/DataUnifier")
        df = pd.read_csv(os.getcwd()+"/Data/DataUnifier/agg_results.csv", index_col=0)
        self.assertGreater(df.shape[0], 60)
        self.assertEquals(df[df['index'] == 30].shape[0], 2)


    def test_extract_files_data_with_protocol(self):
        if os.path.exists(os.getcwd()+"/Data/DataUnifier2/agg_results.csv"):
            os.remove(os.getcwd()+"/Data/DataUnifier2/agg_results.csv")

        unification_protocol_template = [("base", {"ch_type": "Nucli"}),
                                         ("join", {"ch_type": "Cyto", "full_tbl_fld": "cyto_label"}),
                                         ("join", {"ch_type": "Drp1", "full_tbl_fld": "cyto_label"}),
                                         ("join_1_agg", {"ch_type": "MitoTracker", "full_tbl_fld": "cyto_label",
                                                       "largest_key": 'MitoTracker_size'}),

                                         ]
        ImageDataUnifier(os.getcwd()+"/Data/DataUnifier2", unification_protocol=unification_protocol_template)
        df = pd.read_csv(os.getcwd()+"/Data/DataUnifier2/agg_results.csv", index_col=0)
        self.assertGreater(df.shape[0], 60)
        self.assertEquals(df[df['index'] == 5].shape[0], 3)
        self.assertTrue('Drp1_in_MitoTracker_size' in df.columns)
        self.assertTrue('Drp1_outside_MitoTracker_size' in df.columns)
        self.assertTrue('Drp1_in_Nucli_size' in df.columns)
        df.fillna(0, inplace=True)
        self.assertTrue(np.all(df['Drp1_outside_MitoTracker_size']+df['Drp1_in_MitoTracker_size']==df['Drp1_size']))
