import shutil
import unittest
import numpy as np

import os, sys

from Segmenter.FolderLoader import FolderLoader

sys.path.append(os.getcwd() + '/../..')
sys.path.append(os.getcwd() + '/..')
sys.path.append(os.getcwd() + '.')

from Segmenter.CellsImage import CellsImage
from Segmenter.Channels.NucliChannel import NucliChannel
from Segmenter.ImageTools.LenCleaner import LenCleaner

class ImagePreparationTests(unittest.TestCase):

    def test_nuc_segmentation(self):
        image_path = os.getcwd() + "/Data/len_cleaning"
        channels_names = {"B - 02(fld 02 wv DAPI - DAPI).tif": NucliChannel}
        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        ci.Nucli.generate_segmentation()
        nuc = ci.Nucli
        # from Segmenter.ImageTools.GeneralImageTools import show_image
        # nuc.show_image(nuc.segments, cmap='nipy_spectral')
        self.assertEqual(nuc.segments.shape, (2048, 2048))
        self.assertEqual(np.unique(nuc.segments).shape[0], 69)

    def test_stained_image(self):
        image_path = os.getcwd() + "/Data/len_cleaning/stained_image/"
        if os.path.exists(image_path + "results"):
            shutil.rmtree(image_path + "results")

        channels_map = {"DAPI - DAPI": "NucliChannel",
                        "Cy3 - Cy3": ["SemiCytoChannel", "ActinChannel"],
                        "Cy5 - Cy5": "Drp1Channel",
                        "FITC - FITC": "HuntingtinChannel",
                        "DataExtractors": [{"class": "SemiCytoImageUnification",
                                            "data_sources": ["Cyto", "Nucli", "Drp1", "Huntingtin"], "trigger_after": "Nucli"},
                                           {"class": "NucliDataExtractor", "data_sources": ["Nucli"],
                                            "trigger_after": "Nucli"},
                                           {"class": "NucliCytoDataExtractor", "data_sources": ["Nucli", "Cyto"],
                                            "trigger_after": "Nucli"},
                                           {"class": "CytoDataExtractor", "data_sources": ["Cyto"],
                                            "trigger_after": "Cyto"},
                                           {"class": "ActinOverCytoParallelizationDataExtractor", "data_sources": ["Actin", "Cyto", "Nucli"],
                                            "trigger_after": "Actin"},
                                           {"class": "ActinDataExtractor", "data_sources": ["Actin", "Nucli"],
                                            "trigger_after": "Actin"}]}

        FolderLoader(image_path, protocol_channel_map=channels_map, parallel_processing=False)
        self.assertTrue(os.path.exists(image_path + "results"))


