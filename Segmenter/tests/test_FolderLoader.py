import shutil
import unittest
import pandas as pd

import os, sys

sys.path.append(os.getcwd() + '/../..')

from Segmenter.FolderLoader import FolderLoader


class FolderLoaderTests(unittest.TestCase):

    def test_basic_full_cycle(self):
        image_path = os.getcwd() + "/Data/"
        if os.path.exists(os.getcwd() + "/Data/results"):
            shutil.rmtree(os.getcwd() + "/Data/results")
        FolderLoader(image_path, parallel_processing=False)
        self.assertTrue(os.path.exists(os.getcwd() + "/Data/results"))
        res_csv_path = os.getcwd() + "/Data/results/B - 04(fld 04 wv Cy5 - Cy5)-MitoTracker.csv"
        self.assertTrue(os.path.exists(res_csv_path))
        t0 = os.path.getmtime(res_csv_path)
        FolderLoader(image_path, continue_existing_session=True, parallel_processing=2)
        t1 = os.path.getmtime(res_csv_path)
        self.assertEqual(t0, t1)
        # TBD: delete the csv and run again t2 should be different

    def test_basic_full_cycle_with_conf_file(self):
        image_path = os.getcwd() + "/Data/"
        if os.path.exists(os.getcwd() + "/Data/results"):
            shutil.rmtree(os.getcwd() + "/Data/results")
        FolderLoader(image_path, configuration_file=os.getcwd() + '/Data/test_basic_full_cycle_conf.json')
        self.assertTrue(os.path.exists(os.getcwd() + "/Data/results"))
        self.assertTrue(os.path.exists(os.getcwd() + "/Data/results/B - 04(fld 04 wv Cy5 - Cy5)-MitoTracker.csv"))

    def test_multi_ch_per_img(self):
        image_path = os.getcwd() + "/Data/Semi_Cyto_images2/"
        if os.path.exists(image_path + "results"):
            shutil.rmtree(image_path + "results")

        channels_map = {"DAPI - DAPI": "NucliChannel",
                        "Cy5 - Cy5": "Cox17Channel",
                        "Cy3 - Cy3": ["SemiCytoChannel", "ActinChannel"],
                        "DataExtractors": [{"class": "SemiCytoImageUnification", "data_sources": ["Cyto", "Cox17"],
                                            "trigger_after": "Nucli"},
                                           {"class": "NucliDataExtractor", "data_sources": ["NucliChannel"],
                                            "trigger_after": "NucliChannel"},
                                           # {"class": "NucliCytoDataExtractor", "data_sources": ["Nucli", "Cyto"],
                                           #  "trigger_after": "Nucli"},
                                           {"class": "CytoDataExtractor", "data_sources": ["Cyto"],
                                            "trigger_after": "Cyto"}]
                        }

        FolderLoader(image_path, protocol_channel_map=channels_map, parallel_processing=False)
        semi_cyto_data_file = os.getcwd() + "/Data/Semi_Cyto_images2/results/E - 04(fld 13 wv Cy3 - Cy3)-Cyto.csv"
        self.assertTrue(os.path.exists(semi_cyto_data_file))
        df = pd.read_csv(semi_cyto_data_file)
        self.assertEqual(df.shape[0], 21)
        if os.path.exists(image_path + "results"):
            shutil.rmtree(image_path + "results")

    def test_channel_merging(self):
        image_path = os.getcwd() + "/Data/mito_calibration/"
        if os.path.exists(image_path + "results/mito_calibration_D - 11(fld 15 wv DAPI - DAPI).tif"):
            os.remove(image_path + "results/mito_calibration_D - 11(fld 15 wv DAPI - DAPI).tif")
        channels_map = {"DAPI - DAPI": "NucliChannel",
                        "FITC - FITC": "CytoChannel",
                        "DataExtractors": [{"class": "NucliDataExtractor", "data_sources": ["Nucli"],
                                            "trigger_after": "Nucli"},
                                           {"class": "NucliCytoDataExtractor", "data_sources": ["Nucli", "Cyto"],
                                            "trigger_after": "Nucli"},
                                           {"class": "ImageMergeExporter", "data_sources": ["Nucli", "Cyto"],
                                            "trigger_after": "Nucli"}
                                           ]
                        }

        FolderLoader(image_path, protocol_channel_map=channels_map, parallel_processing=False)
        self.assertTrue(os.path.exists(image_path + "results/mito_calibration_D - 11(fld 15 wv DAPI - DAPI).jpg"))
