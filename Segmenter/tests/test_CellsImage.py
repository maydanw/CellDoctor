import json
from unittest import mock

import cv2
import unittest
import numpy as np
import shutil
import pandas as pd

import os, sys

sys.path.append(os.getcwd() + '/../..')
sys.path.append(os.getcwd() + '/..')
sys.path.append(os.getcwd() + '.')

from Segmenter.CellsImage import CellsImage
from Segmenter.Channels.CytoChannel import CytoChannel
from Segmenter.Channels.MitoTrackerChannel import MitoTrackerChannel
from Segmenter.Channels.TMREChannel import TMREChannel
from Segmenter.DataExtractors.CytoDataExtractor import CytoDataExtractor
from Segmenter.Channels.NucliChannel import NucliChannel
from Segmenter.Channels.ActinChannel import ActinChannel
from Segmenter.Channels.SemiCytoChannel import SemiCytoChannel
from Segmenter.Channels.Drp1Channel import Drp1Channel
from Segmenter.Channels.HuntingtinChannel import HuntingtinChannel
from Segmenter.DataExtractors.MitoTrackerDataExtractor import MitoTrackerDataExtractor
from Segmenter.DataExtractors.NucliCytoDataExtractor import NucliCytoDataExtractor
from Segmenter.DataExtractors.NucliDataExtractor import NucliDataExtractor
from Segmenter.DataExtractors.ActinDataExtractor import ActinDataExtractor
from Segmenter.DataExtractors.ActinParallelizationDataExtractor import ActinParallelizationDataExtractor
from Segmenter.DataExtractors.ActinOverCytoParallelizationDataExtractor import ActinOverCytoParallelizationDataExtractor
from Segmenter.DataExtractors.SemiCytoImageUnification import SemiCytoImageUnification
from Segmenter.DataExtractors.TMREDataExtractor import TMREDataExtractor
from Segmenter.ImageTools.GeneralImageTools import save_image
from Segmenter.ImageTools.LenCleaner import LenCleaner
from Segmenter.ImageTools.SegmentationTools import connect_cyto_nucli_labels

rapid_only = False


class CellsImageTests(unittest.TestCase):
    def test_init(self):
        image_path = os.getcwd() + "/Data/"
        channels_names = {"B - 02(fld 01 wv DAPI - DAPI).tif": NucliChannel}
        ci = CellsImage(image_path, channels_names)

        self.assertEqual(ci.Nucli.base_image.shape, (2048, 2048))
        self.assertEqual(ci.channels["Nucli"].base_image.shape, (2048, 2048))
        # ci.channels["Nucli"].show_image()

    def test_init_multiple_ch_per_img(self):
        image_path = os.getcwd() + "/Data/"
        channels_names = {"B - 02(fld 01 wv DAPI - DAPI).tif": [NucliChannel, CytoChannel]}
        ci = CellsImage(image_path, channels_names)

        self.assertEqual(ci.Nucli.base_image.shape, (2048, 2048))
        self.assertEqual(ci.Cyto.base_image.shape, (2048, 2048))

    def test_nuc_segmentation(self):
        image_path = os.getcwd() + "/Data/"
        channels_names = {"B - 02(fld 01 wv DAPI - DAPI).tif": NucliChannel}
        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        ci.Nucli.generate_segmentation()
        nuc = ci.Nucli
        # from Segmenter.ImageTools.GeneralImageTools import show_image
        # nuc.show_image(nuc.segments, cmap='nipy_spectral')
        self.assertEqual(nuc.segments.shape, (2048, 2048))
        self.assertEqual(np.unique(nuc.segments).shape[0], 33)

    def test_nuc_data_extraction(self):
        image_path = os.getcwd() + "/Data/"
        channels_names = {"B - 02(fld 01 wv DAPI - DAPI).tif": NucliChannel}
        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        ci.Nucli.generate_segmentation()
        NucliDataExtractor(ci.Nucli).do()
        nuc_data = ci.Nucli.data
        self.assertEqual(31, nuc_data[nuc_data.nuc_outlier == False].shape[0])
        self.assertEqual(1, nuc_data[nuc_data.nuc_outlier == True].shape[0], 1)
        self.assertTrue("Nucli_morphological_cx" in nuc_data.columns)

    def test_nuc_cyto_data_extractor(self):
        image_path = os.getcwd() + "/Data/"
        channels_names = {"B - 02(fld 01 wv DAPI - DAPI).tif": NucliChannel,
                          "B - 02(fld 01 wv FITC - FITC).tif": CytoChannel}

        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        ci.Nucli.generate_segmentation()
        NucliDataExtractor(ci.Nucli).do()
        NucliCytoDataExtractor(ci.Nucli, ci.Cyto).do()
        self.assertTrue("nuc_outlier" in ci.Nucli.data.columns)
        self.assertEqual(ci.Nucli.data.nuc_outlier.dtype, np.dtype('bool'))
        outliers = ci.Nucli.data.nuc_outlier.index[ci.Nucli.data.nuc_outlier].tolist()
        # self.assertListEqual(outliers, [5, 9, 11, 23])
        self.assertListEqual(outliers, [9, 23])

        # mask = np.zeros( ci.Nucli.base_image.shape,np.uint8)
        # for idx in outliers:
        #     mask[ci.Nucli.segments==idx] = 255
        # zipped = np.dstack((mask, ci.Cyto.base_image, ci.Nucli.base_image))
        # show_image(zipped)

    @unittest.skipIf(rapid_only, "rapid_only")
    def test_cyto_data_extractor(self):
        image_path = os.getcwd() + "/Data/"
        channels_names = {"B - 02(fld 01 wv DAPI - DAPI).tif": NucliChannel,
                          "B - 02(fld 01 wv FITC - FITC).tif": CytoChannel}
        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        ci.Nucli.generate_segmentation()
        NucliDataExtractor(ci.Nucli).do()
        NucliCytoDataExtractor(ci.Nucli, ci.Cyto).do()

        # These two option are exchangeable.
        # Option A:
        # ----------
        ci.Cyto.generate_segmentation()
        #
        # Option B:
        # ----------
        # ci.Cyto.segments = cv2.imread(os.getcwd()+"/Data/B - 02 fld 01 Cyto segments.tif", cv2.IMREAD_UNCHANGED)
        # connect_cyto_nucli_labels(ci.Cyto.segments, ci.Cyto.data, ci.Nucli)

        self.assertTrue(ci.Nucli.data[ci.Nucli.data.nuc_outlier].cyto_label.isna().all())
        self.assertFalse(ci.Nucli.data[ci.Nucli.data.nuc_outlier == False].cyto_label.isna().any())
        self.assertFalse(ci.Cyto.data.nucli_label.isna().any())
        CytoDataExtractor(ci.Cyto).do()
        self.assertEqual(np.unique(ci.Cyto.segments).shape[0] + ci.Nucli.data[ci.Nucli.data.nuc_outlier].shape[0],
                         np.unique(ci.Nucli.segments).shape[0])
        self.assertTrue(ci.Cyto.data.shape[1] > 20)

    def test_cyto_data_extractor_w_empty_nucli(self):

        with mock.patch('cv2.imread') as mock_imread:
            mock_imread.return_value = np.zeros([2048, 2048], dtype=np.uint8)

            image_path = os.getcwd() + "/Data/"
            channels_names = {"B - 02(fld 01 wv DAPI - DAPI).tif": NucliChannel,
                              "B - 02(fld 01 wv FITC - FITC).tif": CytoChannel}
            ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        ci.Nucli.generate_segmentation()
        NucliDataExtractor(ci.Nucli).do()
        NucliCytoDataExtractor(ci.Nucli, ci.Cyto).do()

        ci.Cyto.generate_segmentation()

        self.assertEqual(ci.Nucli.data.shape[0], 0)
        self.assertEqual(ci.Cyto.data.shape[0], 0)

    def test_mito_data_segmentation(self):
        image_path = os.getcwd() + "/Data/"
        channels_names = {"B - 02(fld 01 wv DAPI - DAPI).tif": NucliChannel,
                          "B - 02(fld 01 wv FITC - FITC).tif": CytoChannel,
                          'B - 02(fld 01 wv Cy5 - Cy5).tif': MitoTrackerChannel,
                          'B - 02(fld 01 wv Cy3 - Cy3).tif': TMREChannel,
                          }
        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        ci.Nucli.generate_segmentation()
        NucliDataExtractor(ci.Nucli).do()
        NucliCytoDataExtractor(ci.Nucli, ci.Cyto).do()

        ci.Cyto.segments = cv2.imread(os.getcwd() + "/Data/B - 02 fld 01 Cyto segments.tif", cv2.IMREAD_UNCHANGED)
        connect_cyto_nucli_labels(ci.Cyto.segments, ci.Cyto.data, ci.Nucli)

        # ci.Cyto.connect_cyto_nucli_labels(ci.Cyto.segments, ci.Nucli) #Maydan
        CytoDataExtractor(ci.Cyto).do()

        self.assertTrue(ci.Nucli.data[ci.Nucli.data.nuc_outlier].cyto_label.isna().all())
        self.assertFalse(ci.Nucli.data[ci.Nucli.data.nuc_outlier == False].cyto_label.isna().any())
        self.assertFalse(ci.Cyto.data.nucli_label.isna().any())
        self.assertTrue(ci.Cyto.data.shape[1] > 20)

        ci.MitoTracker.generate_segmentation()
        self.assertFalse(ci.MitoTracker.data.cyto_label.isna().any())
        MitoTrackerDataExtractor(ci.MitoTracker).do()

        ci.TMRE.generate_segmentation()
        TMREDataExtractor(ci.TMRE).do()

        if os.path.exists(os.getcwd() + "/Data/results"):
            shutil.rmtree(os.getcwd() + "/Data/results")
        ci.save_data()
        self.assertTrue(os.path.exists(os.getcwd() + "/Data/results"))
        self.assertTrue(os.path.exists(os.getcwd() + "/Data/results/B - 02(fld 01 wv Cy5 - Cy5)-MitoTracker.csv"))
        df = pd.read_csv(os.getcwd() + "/Data/results/B - 02(fld 01 wv Cy5 - Cy5)-MitoTracker.csv", index_col=0)
        self.assertEqual(df.shape, ci.MitoTracker.data.shape)

    def test_mito_calibration(self):
        image_path = os.getcwd() + "/Data/mito_calibration/"
        channels_names = {"D - 11(fld 15 wv DAPI - DAPI).tif": NucliChannel,
                          "D - 11(fld 15 wv FITC - FITC).tif": CytoChannel,
                          'D - 11(fld 15 wv Cy5 - Cy5).tif': MitoTrackerChannel,
                          'D - 11(fld 15 wv Cy3 - Cy3).tif': TMREChannel,
                          }
        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        ci.Nucli.generate_segmentation()
        NucliDataExtractor(ci.Nucli).do()
        NucliCytoDataExtractor(ci.Nucli, ci.Cyto).do()
        ci.Cyto.generate_segmentation()
        CytoDataExtractor(ci.Cyto).do()
        ci.MitoTracker.generate_segmentation()
        MitoTrackerDataExtractor(ci.MitoTracker).do()

        ci.TMRE.generate_segmentation()
        TMREDataExtractor(ci.TMRE).do()
        self.assertGreater(ci.MitoTracker.data.cyto_label[ci.MitoTracker.data.cyto_label == 8].count(), 20)

    def test_actin_data_extractor(self):
        image_path = os.getcwd() + '/Data/Semi_Cyto_images/'
        channels_names = {'A - 03(fld 18 wv Cy3 - Cy3).tif': [ActinChannel, SemiCytoChannel],
                          'A - 03(fld 18 wv DAPI - DAPI).tif': NucliChannel,
                          'A - 03(fld 18 wv Cy5 - Cy5).tif': Drp1Channel,
                          'A - 03(fld 18 wv FITC - FITC).tif': HuntingtinChannel}

        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)
        SemiCytoImageUnification(ci.Cyto, ci.Nucli, ci.Drp1, ci.Huntingtin).do()

        ci.Nucli.generate_segmentation()

        NucliDataExtractor(ci.Nucli).do()
        NucliCytoDataExtractor(ci.Nucli, ci.Cyto).do()

        ci.Cyto.generate_segmentation()
        CytoDataExtractor(ci.Cyto).do()

        ci.Actin.generate_segmentation()
        ActinDataExtractor(ci.Actin, ci.Nucli).do()

        self.assertIsNotNone(ci.Actin.data)
        self.assertEqual(ci.Actin.data.shape, (16, 6))
        self.assertTrue('Actin_number_of_actin_pixels_over_nucli' in ci.Actin.data.columns)

    def test_actin_over_cyto_parallelization_data_extractor(self):
        image_path = os.getcwd() + '/Data/Semi_Cyto_images/'
        channels_names = {'A - 03(fld 18 wv Cy3 - Cy3).tif': [ActinChannel, SemiCytoChannel],
                          'A - 03(fld 18 wv DAPI - DAPI).tif': NucliChannel,
                          'A - 03(fld 18 wv Cy5 - Cy5).tif': Drp1Channel,
                          'A - 03(fld 18 wv FITC - FITC).tif': HuntingtinChannel}

        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)
        SemiCytoImageUnification(ci.Cyto, ci.Nucli, ci.Drp1, ci.Huntingtin).do()

        ci.Nucli.generate_segmentation()

        NucliDataExtractor(ci.Nucli).do()
        NucliCytoDataExtractor(ci.Nucli, ci.Cyto).do()

        ci.Cyto.generate_segmentation()
        CytoDataExtractor(ci.Cyto).do()

        ci.Actin.generate_segmentation()
        ActinOverCytoParallelizationDataExtractor(ci.Actin, ci.Cyto, ci.Nucli).do()

        self.assertIsNotNone(ci.Actin.data)
        self.assertEqual((16, 2), ci.Actin.data.shape)
        self.assertTrue('Actin_fibres_slope_std' in ci.Actin.data.columns)

    def test_actin_parallelization_data_extractor(self):
        image_path = os.getcwd() + '/Data/Semi_Cyto_images/'
        channels_names = {'A - 03(fld 18 wv DAPI - DAPI).tif': NucliChannel,
                          'A - 03(fld 18 wv Cy3 - Cy3).tif': ActinChannel,
                          }

        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        ci.Nucli.generate_segmentation()
        ActinParallelizationDataExtractor(ci.Actin, ci.Nucli).do()

        self.assertIsNotNone(ci.Actin.data)
        self.assertEqual((30, 2), ci.Actin.data.shape)
        self.assertTrue('Actin_fibres_slope_std' in ci.Actin.data.columns)

    def test_semicyto_segmentation(self):
        image_path = os.getcwd() + '/Data/Semi_Cyto_images/'
        channels_names = {'A - 03(fld 18 wv Cy3 - Cy3).tif': [ActinChannel, SemiCytoChannel],
                          'A - 03(fld 18 wv DAPI - DAPI).tif': NucliChannel,
                          'A - 03(fld 18 wv Cy5 - Cy5).tif': Drp1Channel,
                          'A - 03(fld 18 wv FITC - FITC).tif': HuntingtinChannel}

        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)
        SemiCytoImageUnification(ci.Cyto, ci.Nucli, ci.Drp1, ci.Huntingtin).do()

        ci.Nucli.generate_segmentation()
        NucliDataExtractor(ci.Nucli).do()
        NucliCytoDataExtractor(ci.Nucli, ci.Cyto).do()

        ci.Cyto.generate_segmentation()

        self.assertTrue(np.shape(ci.Cyto.segments), (2048, 2048))
        self.assertTrue(len(np.unique(ci.Cyto.segments)), 31)
        # 31 segments including 0 =>>> 30 cyto segments overall

    def test_semicyto_segmentation_with_data_extraction_protocol(self):
        image_path = os.getcwd() + '/Data/Semi_Cyto_images/'
        channels_names = {'A - 02(fld 01 wv Cy3 - Cy3).tif': [ActinChannel, SemiCytoChannel],
                          'A - 02(fld 01 wv DAPI - DAPI).tif': NucliChannel,
                          'A - 02(fld 01 wv Cy5 - Cy5).tif': Drp1Channel,
                          'A - 02(fld 01 wv FITC - FITC).tif': HuntingtinChannel,
                          }
        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        extractor_protocol = [{"class": "SemiCytoImageUnification",
                               "data_sources": ["Cyto", "Nucli", "Drp1", "Huntingtin"],
                               "trigger_after": "Nucli"},
                              {"class": "NucliDataExtractor", "data_sources": ["Nucli"],
                               "trigger_after": "Nucli"},
                              {"class": "NucliCytoDataExtractor", "data_sources": ["Nucli", "Cyto"],
                               "trigger_after": "Nucli"},
                              {"class": "CytoDataExtractor", "data_sources": ["Cyto"],
                               "trigger_after": "Cyto"},
                              {"class": "AntiBodyDataExtractor", "data_sources": ["Drp1"],
                               "trigger_after": "Drp1"},
                              {"class": "AntiBodyCoLocalizationDataExtractor", "data_sources": ["Drp1", "Nucli"],
                               "trigger_after": "Drp1"},
                              {"class": "AntiBodyDataExtractor", "data_sources": ["Huntingtin"],
                               "trigger_after": "Huntingtin"}
                              ]

        ci.process_cell_image(extractor_protocol)

        self.assertTrue(np.shape(ci.Cyto.segments), (2048, 2048))
        self.assertTrue(len(np.unique(ci.Cyto.segments)), 18)
        self.assertTrue(np.all(
            ci.Drp1.data['Drp1_in_Nucli_size'] + ci.Drp1.data['Drp1_outside_Nucli_size'] == ci.Drp1.data[
                'Drp1_intensity_count']))
        # 18 segments including 0 =>>> 17 cyto segments overall

    def test_ContoursExporter(self):
        image_path = os.getcwd() + '/Data/'
        contours_file_path = image_path + 'B - 02(fld 01 wv FITC - FITC).json'

        if os.path.exists(contours_file_path):
            os.remove(contours_file_path)

        channels_names = {"B - 02(fld 01 wv DAPI - DAPI).tif": NucliChannel,
                          "B - 02(fld 01 wv FITC - FITC).tif": CytoChannel,
                          'B - 02(fld 01 wv Cy5 - Cy5).tif': MitoTrackerChannel,
                          'B - 02(fld 01 wv Cy3 - Cy3).tif': TMREChannel,
                          }
        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        extractor_protocol = [{"class": "NucliDataExtractor", "data_sources": ["Nucli"],
                               "trigger_after": "Nucli"},
                              {"class": "NucliCytoDataExtractor", "data_sources": ["Nucli", "Cyto"],
                               "trigger_after": "Nucli"},
                              {"class": "CytoDataExtractor", "data_sources": ["Cyto"],
                               "trigger_after": "Cyto"},
                              {"class": "ContoursExporter", "data_sources": ["Cyto"],
                               "trigger_after": "Cyto"},
                              ]

        ci.process_cell_image(extractor_protocol)

        self.assertTrue(os.path.exists(contours_file_path))

        with open(contours_file_path, 'rb') as f:
            contours_data = json.load(f)
        self.assertEqual(len(contours_data['shapes']), 30)


class IntegrationTestsLong(unittest.TestCase):
    def test_cyto_segmentation(self):
        image_path = os.getcwd() + "/Data/"
        channels_names = {"B - 02(fld 01 wv DAPI - DAPI).tif": NucliChannel,
                          "B - 02(fld 01 wv FITC - FITC).tif": CytoChannel}
        ci = CellsImage(image_path, channels_names)

        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)

        ci.Nucli.generate_segmentation()
        NucliDataExtractor(ci.Nucli).do()
        NucliCytoDataExtractor(ci.Nucli, ci.Cyto).do()
        ci.Cyto.generate_segmentation()

        # from Segmenter.ImageTools.GeneralImageTools import show_image, save_image
        # show_image(ci.Cyto.segments, cmap = 'nipy_spectral')
        # save_image(ci.Cyto.segments, './Data/B - 02 fld 01 Cyto segments.tif')
        # save_image(ci.Nucli.segments, './Data/B - 02 fld 01 Nucli segments.tif')
        self.assertEqual(np.unique(ci.Cyto.segments).shape[0] + ci.Nucli.data[ci.Nucli.data.nuc_outlier].shape[0],
                         np.unique(ci.Nucli.segments).shape[0])
        self.assertEqual(np.unique(ci.Cyto.segments).shape[0], 31)  # 32 + black
        self.assertTrue('cyto_label' in ci.Nucli.data.columns)
        self.assertTrue(ci.Nucli.data[ci.Nucli.data.nuc_outlier].cyto_label.isna().all())
        self.assertFalse(ci.Nucli.data[ci.Nucli.data.nuc_outlier == False].cyto_label.isna().any())
        self.assertFalse(ci.Cyto.data.nucli_label.isna().any())

#
# class VisualIntegrationTestsLong(unittest.TestCase):
#
#     def test_full_cycle_temp3(self):
#         image_path = "C:\\Users\\mayda\\Downloads\\"
#         channels_names = {"D - 03(fld 10 wv DAPI - DAPI).tif": NucliChannel,
#                           "D - 03(fld 10 wv FITC - FITC).tif": CytoChannel,
#                           'D - 03(fld 10 wv Cy5 - Cy5).tif': MitoTrackerChannel,
#                           'D - 03(fld 10 wv Cy3 - Cy3).tif': TMREChannel,
#                           }
#         ci = CellsImage(image_path, channels_names)
#
#         len_cleaner = LenCleaner(ci.Nucli.base_image)
#         ci.run_image_processor(len_cleaner)
#
#         ci.Nucli.generate_segmentation()
#         NucliDataExtractor(ci.Nucli).do()
#         NucliCytoDataExtractor(ci.Nucli, ci.Cyto).do()
#
#         ci.Cyto.generate_segmentation()
#         CytoDataExtractor(ci.Cyto).do()
#
#         ci.MitoTracker.generate_segmentation()
#         MitoTrackerDataExtractor(ci.MitoTracker).do()
#
#         ci.TMRE.generate_segmentation()
#         TMREDataExtractor(ci.TMRE).do()
#         print("done")
#
#     def test_full_cycle_temp4(self):
#         image_path = "C:/BioData/HD/Standard lab acquisition protocol 5_D.CAL.TM.MITO MIX1 24H+SER_1/"
#         channels_names = {"B - 03(fld 03 wv DAPI - DAPI).tif": NucliChannel,
#                           "B - 03(fld 03 wv FITC - FITC).tif": CytoChannel,
#                           'B - 03(fld 03 wv Cy5 - Cy5).tif': MitoTrackerChannel,
#                           'B - 03(fld 03 wv Cy3 - Cy3).tif': TMREChannel,
#                           }
#         ci = CellsImage(image_path, channels_names)
#
#         len_cleaner = LenCleaner(ci.Nucli.base_image)
#         ci.run_image_processor(len_cleaner)
#
#         ci.Nucli.generate_segmentation()
#         NucliDataExtractor(ci.Nucli).do()
#         NucliCytoDataExtractor(ci.Nucli, ci.Cyto).do()
#
#         ci.Cyto.generate_segmentation()
#         CytoDataExtractor(ci.Cyto).do()
#
#         ci.MitoTracker.generate_segmentation()
#         MitoTrackerDataExtractor(ci.MitoTracker).do()
#
#         ci.TMRE.generate_segmentation()
#         TMREDataExtractor(ci.TMRE).do()
#         print("done")
