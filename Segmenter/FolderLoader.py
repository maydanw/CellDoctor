import os
import shutil
from glob import glob
import re
import pandas as pd
import json
from dask import delayed

from Segmenter.CellsImage import CellsImage
from Segmenter.Constants import default_channel_map
from Segmenter.ImageTools.SegmentationTools import load_class
from Segmenter.ImageTools.LenCleaner import LenCleaner


class FolderLoader:

    def __init__(self, folder_path: str, continue_existing_session: bool = False, configuration_file=None,
                 protocol_channel_map=None, parallel_processing=None):

        conf_data = {}

        if configuration_file is not None:
            with open(configuration_file) as json_data:
                conf_data = json.load(json_data)

        self.channel_map = {}
        self.data_extractors_protocol = []
        self.load_protocol_channel_map(conf_data, protocol_channel_map)

        self.folder_path = ""
        self.load_folder_path(conf_data, folder_path)

        if os.path.exists(self.folder_path):
            if not os.path.exists(self.folder_path + "/results/meta"):
                os.makedirs(self.folder_path + "/results/meta")
            with open(self.folder_path + "/results/meta/protocol.json", 'w') as outfile:
                output_protocol = {}
                if configuration_file:
                    output_protocol = configuration_file
                if protocol_channel_map:
                    output_protocol = protocol_channel_map
                json.dump(output_protocol, outfile, sort_keys=True, indent=4)
            git_path = os.path.abspath(os.path.dirname(__file__)) + "/../.git/ORIG_HEAD"
            if os.path.exists(git_path):
                shutil.copy(git_path, self.folder_path + "/results/meta/git_version.txt")

        self.parallel_processing = parallel_processing
        if self.parallel_processing is None:
            self.parallel_processing = conf_data.get('parallel_processing', 2)
        if type(self.parallel_processing) == bool:
            if self.parallel_processing:
                self.parallel_processing = 2
            else:
                self.parallel_processing = 0

        files = glob(self.folder_path + "*.tif")

        self.data = self.extract_files_data(files)
        grp = self.data.groupby(by=["row", "column", "field"])
        res = []
        for img_k in grp.groups:
            img_df = grp.get_group(img_k)

            channels_names = {}
            for i, row in img_df.iterrows():
                if self.channel_map.get(row.channel) is None:
                    continue
                channels_names[row.name] = self.channel_map[row.channel]

            process_file = self.process_file_check(channels_names, continue_existing_session, self.folder_path)
            if not process_file:
                continue

            if self.parallel_processing:
                res.append(delayed(self.run_workflow)(channels_names, img_k))
            else:
                self.run_workflow(channels_names, img_k)
        if self.parallel_processing:
            result = delayed(list)(res)
            result.compute(scheduler='processes', num_workers=self.parallel_processing)

    def run_workflow(self, channels_names, img_k):
        ci = CellsImage(self.folder_path, channels_names)
        len_cleaner = LenCleaner(ci.Nucli.base_image)
        ci.run_image_processor(len_cleaner)
        print(f'Loaded {img_k}')
        ci.process_cell_image(self.data_extractors_protocol)

    def load_folder_path(self, conf_data, folder_path):
        if 'folder_path' in conf_data:
            folder_path = conf_data['folder_path']
        if os.path.exists(folder_path):
            self.folder_path = folder_path
        else:
            raise RuntimeError(f"Could not find path: {folder_path}")
        if self.folder_path[-1] not in ['/', '\\']:
            self.folder_path = self.folder_path + '/'

    def load_protocol_channel_map(self, conf_data, protocol_channel_map):
        if 'protocol_channel_map' in conf_data:
            protocol_channel_map = conf_data['protocol_channel_map']
        if protocol_channel_map is None:
            protocol_channel_map = default_channel_map
        for ch_mark, ch_types in protocol_channel_map.items():
            if ch_mark == "DataExtractors":
                self.data_extractors_protocol = protocol_channel_map["DataExtractors"]
                continue
            if type(ch_types) is not list:
                ch_types = [ch_types]
            self.channel_map[ch_mark] = []
            for ch_type in ch_types:
                cls = load_class(ch_type)
                self.channel_map[ch_mark].append(cls)

    def process_file_check(self, channels_names, continue_session, folder_path):
        if not continue_session:
            return True

        if folder_path[-1] not in ['/', '\\']:
            folder_path = folder_path + '/'

        process_file = False
        for ch_k, ch_v in channels_names.items():
            for ch in ch_v:
                filename = os.path.basename(ch_k)
                filename = filename[:filename.rfind('.')] + "-" + ch.name + ".csv"

                if not os.path.exists(folder_path + 'results/' + filename):
                    process_file = True
        return process_file

    def extract_files_data(self, files):
        p = re.compile("(.*) - (.*)\(fld (.*) wv (.*)\).*.tif")
        tuple_list = []
        index = []
        for fn in files:
            filename = os.path.basename(fn)
            m = p.match(filename)
            if m is None:
                print(f"filename: {fn} could not be decoded by pattern {p.pattern}")
                continue
            pattern_res = m.groups()
            if len(pattern_res) != 4:
                print(f"filename: {fn} decoding does not fit expectation of 4 arguments")
                continue
            tuple_list.append(pattern_res)
            index.append(fn)
        df = pd.DataFrame.from_records(tuple_list, columns=["row", "column", "field", "channel"], index=index)
        return df
