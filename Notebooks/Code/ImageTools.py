import cv2
from glob import glob
import matplotlib.pyplot as plt


def show_image(path, cmap='nipy_spectral'):
    """Show an image or a group of images
    cmap values: ['gray', 'jet', 'nipy_spectral']"""
    files = glob(path)
    images = {}
    for fn in files:
        try:
            images[fn]=cv2.imread(fn, cv2.IMREAD_UNCHANGED)
        except Exception as err:
            print(err)

    number_of_images = len(images)
    fig, axeslist = plt.subplots(ncols=1, nrows=number_of_images, figsize=(10, 10 * number_of_images))


    for ind,title in zip(range(number_of_images), images.keys()):
        if number_of_images==1:
            axeslist.imshow(images[title], cmap=cmap)
            axeslist.set_title(title)
            axeslist.set_axis_off()
            break

        axeslist[ind].imshow(images[title], cmap=cmap)
        axeslist[ind].set_title(title)
        axeslist[ind].set_axis_off()
    plt.tight_layout()

    plt.axis('off')
    plt.tight_layout()
    return

