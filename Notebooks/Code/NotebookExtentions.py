import time

from IPython.core.display import display, HTML, clear_output

import json
from matplotlib.patches import Polygon
from matplotlib.colors import to_rgba
import matplotlib.pyplot as plt

import numpy as np


def msg(msg, level='info'):
    """
    Return a nicely looking massage in the notebook
    :param msg:
    :param level: [success, danger, warning, primary, secondary, dark]
    :return:
    """

    if level not in ['success', 'danger', 'warning', 'primary', 'secondary', 'dark', 'info']:
        level = 'dark'
    return HTML(f'''<div class="alert alert-{level}" role="alert">
      {msg}  
    </div>''')


def reporter(steps, active=None, disabled=None):
    if disabled is None:
        disabled = []
    if type(disabled) == str:
        disabled = [disabled]
    s = '<ul class="list-group">'
    for step in steps:
        s = s + f'<li class="list-group-item {"active" if active == step else ""} {"disabled" if step in disabled else ""}">{step}</li>'
    s = s + '</ul>'
    clear_output(wait=True)
    return display(HTML(s))


def action_runner(actions, df, skip):
    action_list = [a[0] for a in actions]
    action_list.append("Done")
    for act in actions:
        if act[0] not in skip:
            df = act[1](df)
            reporter(action_list, active=act[0], disabled=skip)
        time.sleep(0.2)
    reporter(action_list, active="Done", disabled=skip)
    return df


def load_img_and_segmentation(img_path, seg_path):
    img = plt.imread(img_path)

    with open(seg_path, 'r') as f:
        segmentation_data = json.load(f)

    fig, ax = plt.subplots(1, 1)

    ax.imshow(img)

    for shape in segmentation_data['shapes']:
        points = np.array(shape['points'])
        color = to_rgba(np.array(shape['fill_color']) / 255)
        ax.add_patch(Polygon(points, True, linewidth=2, edgecolor=color, facecolor=color, alpha=0.5))

    plt.show()
