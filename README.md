# CellDoctor
---

## Prepare
You should clone this repository

    git clone https://gitlab.com/maydanw/CellDoctor.git

and then install necessary packages.

#### a) Create a conda environment (preferred)
In the repo directory

    conda env create -f environment.yml
    conda activate cell-doctor


#### b) Use Dockerfile

You can build a docker image out of the provided Dockerfile.

## How to run the image analysis part
```python
import sys
segmenter_path = "../"
sys.path.append(segmenter_path) 

from Segmenter.FolderLoader import FolderLoader

# Replace the path
images_path = 'C:\\BioData\\HD\\Standard lab acquisition protocol 5_D.CAL.TM.MITO MIX1 24H+SER_1\\'

FolderLoader(images_path) 
```

## How to run the data unification part
```python
import sys
segmenter_path = "../"
sys.path.append(segmenter_path) 

from Segmenter.DataProcessor.ImageDataUnifier import ImageDataUnifier 


# Replace the path
images_path = 'C:\\BioData\\HD\\Standard lab acquisition protocol 5_D.CAL.TM.MITO MIX1 24H+SER_1\\'

ImageDataUnifier(images_path + 'results\\')
print('Done')

```

